﻿$(document).ready(function () {
    $.noConflict();
    $('#tblFeedback').DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "ordering": true,
        "order": [[3, "desc"]],
        dom: 'lBfrtip',
        "ajax": {
            "dataType": 'json',
            "type": "POST",
            "url": '/ApplicationUsers/FeedbackJson',
            "dataSrc": function (json) {
                return json.data.Items;
            }
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        pageLength: 10,
        sPaginationType: "full_numbers",
        buttons: [
            {
                extend: 'excel',
                text: 'Download'
            },       
        ],
        columns: [
            { data: "UserNumber", },
            { data: "CategoryName" },
            { data: "Text", bSortable: false, },
            { data: "CreatedDate" }
        ]
    })
});


