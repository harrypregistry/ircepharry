﻿var paginationVM = {
    'pageIndex': 0,
    'totalPages': 0,
    'maxValue': 0,
    'pageEnd': 0,
    'pageStart': 0
};

paginationVM.init = function (totalpages, maxvalue = 5) {
    if (totalpages > 0) {
        paginationVM.pageStart = 1;
        paginationVM.pageIndex = 1;
        paginationVM.totalPages = totalpages;
        paginationVM.maxValue = maxvalue;
        paginationVM.pageEnd = totalpages > 5 ? paginationVM.pageIndex + (maxvalue - 1) : totalpages;
        paginationVM.initList();
    } else {
        paginationVM.clearNav();
    }
};

paginationVM.getPage = function (page, totalPages) {

    var prevAnchor = document.getElementById("aprev");
    var nextAnchor = document.getElementById("anext");
    var prevLi = document.getElementById("liprev");
    var nextLi = document.getElementById("linext");
    var lastLi = document.getElementById("li" + paginationVM.pageIndex);
    var currentLi = document.getElementById("li" + page);

    if (paginationVM.pageStart === page) {
        if (page === 1) {
            prevLi.className = "";
            prevLi.className = "page-item disabled";
            prevAnchor.setAttribute("data-id", page);
        } else {
            prevLi.className = "";
            prevLi.className = "page-item";
            prevAnchor.setAttribute("data-id", page);
        }

        nextLi.className = "";
        nextLi.className = "page-item";
        nextAnchor.setAttribute("data-id", page);

        lastLi.className = "";
        lastLi.className = "page-item";

        currentLi.className = "";
        currentLi.className = "page-item active";
        paginationVM.pageIndex = page;
    } else if (paginationVM.pageEnd === page) {

        if (page === totalPages) {
            nextLi.className = "";
            nextLi.className = "page-item disabled";
            nextAnchor.setAttribute("data-id", page);
        } else {
            nextLi.className = "";
            nextLi.className = "page-item";
            nextAnchor.setAttribute("data-id", page);
        }

        prevLi.className = "";
        prevLi.className = "page-item";
        prevAnchor.setAttribute("data-id", page);


        lastLi.className = "";
        lastLi.className = "page-item";

        currentLi.className = "";
        currentLi.className = "page-item active";
        paginationVM.pageIndex = page;
    } else if (paginationVM.pageEnd > page && paginationVM.pageStart < page) {
        prevLi.className = "";
        prevLi.className = "page-item";
        prevAnchor.setAttribute("data-id", page);

        nextLi.className = "";
        nextLi.className = "page-item";
        nextAnchor.setAttribute("data-id", page);

        lastLi.className = "";
        lastLi.className = "page-item";

        currentLi.className = "";
        currentLi.className = "page-item active";
        paginationVM.pageIndex = page;
    } else if (paginationVM.pageEnd < page) {
        var pageEndValue = page + (paginationVM.maxValue - 1);
        paginationVM.pageStart = page;
        paginationVM.pageEnd = pageEndValue <= totalPages ? pageEndValue : totalPages;
        paginationVM.pageIndex = page;
        paginationVM.totalPages = totalPages;
        paginationVM.initList();
    } else if (paginationVM.pageStart > page) {
        var pageStartValue = page - (paginationVM.maxValue - 1);
        paginationVM.pageStart = pageStartValue > 0 ? pageStartValue : 1;
        paginationVM.pageEnd = page;
        paginationVM.pageIndex = page;
        paginationVM.totalPages = totalPages;
        paginationVM.initList();
    }

};

paginationVM.initList = function () {

    paginationVM.clearNav();
    var navEl = "";
    navEl += "<nav>";
    navEl += "<ul class=\"pagination\">";
    navEl += paginationVM.prevList();
    if (paginationVM.pageStart > paginationVM.maxValue) {
        var pagestart = paginationVM.pageStart;
        navEl += paginationVM.createDotList(pagestart - 1);
    }
    for (var i = paginationVM.pageStart; i <= paginationVM.pageEnd; i++) {
        navEl += paginationVM.createList(i);
    }
    if (paginationVM.totalPages > paginationVM.pageEnd) {
        var pageend = paginationVM.pageEnd;
        navEl += paginationVM.createDotList(pageend + 1);
    }
    navEl += paginationVM.nextList();
    navEl += "</ul>";
    navEl += "</nav>";
    paginationVM.add(navEl);
};

paginationVM.createDotList = function (value) {
    return "<li id=\"li" + value + "\" class=\"page-item\"><a id=\"a" + value + "\" data-id=\"" + value + "\" data-info=\"index\" class=\"page-link\" href=\"#\">...</a></li>";
};


paginationVM.createList = function (value) {
    var classActive = "";
    if (paginationVM.pageIndex === value) {
        classActive = "active";
    }
    return "<li id=\"li" + value + "\" class=\"page-item " + classActive + "\"><a id=\"a" + value + "\" data-id=\"" + value + "\" data-info=\"index\" class=\"page-link\" href=\"#\">" + value + "</a></li>";
};

paginationVM.prevList = function () {
    var prevClass = "";
    if (paginationVM.pageIndex === 1) {
        prevClass = "disabled";
    }
    return "<li id=\"liprev\" class=\"page-item " + prevClass + "\"><a id=\"aprev\" data-id=\"" + paginationVM.pageIndex + "\" data-info=\"prev\" class=\"page-link\" href=\"#\">Previous</a></li>";
};

paginationVM.nextList = function () {
    var nextClass = "";
    if (paginationVM.pageIndex === paginationVM.totalPages) {
        nextClass = "disabled";
    }
    return "<li id=\"linext\" class=\"page-item " + nextClass + "\"><a id=\"anext\" data-id=\"" + paginationVM.pageIndex + "\" data-info=\"next\" class=\"page-link\" href=\"#\">Next</a></li>";
};



paginationVM.clearNav = function () {
    $('#custom-navbar').empty();
};

paginationVM.add = function (navEl) {
    $('#custom-navbar').append(navEl);
};


