﻿

function createHabitTableBody(items) {

    if (items.length > 0) {
        var tableRow = "";
        tableRow += "<tbody>";

        for (var i = 0; i < items.length; i++) {

            var item = items[i];

            var startMedicine = parseInt(item.StartMedicine);
            var stopMedicine = -1;
            if (item.StopMedicine !== "") {
                stopMedicine = parseInt(item.StopMedicine);
            }

            tableRow += "<tr>";

            tableRow += "<td class=\"text-center font-weight-bold\">";
            tableRow += item.Search;
            tableRow += "</td>";


            tableRow += "<td class=\"text-center font-weight-bold\">";
            tableRow += item.Drugs;
            tableRow += "</td>";


            for (var index = 0; index <= 4; index++) {
                if (startMedicine === stopMedicine) {
                    if (startMedicine === index && stopMedicine === index) {
                        tableRow += "<td class=\"text-center font-weight-bold\">";
                        tableRow += "<span class=\"text-success\">X</span>";
                        tableRow += "<span class=\"text-danger\">X</span>";
                        tableRow += "</td>";
                    } else {
                        tableRow += "<td></td>";
                    }
                } else {
                    if (startMedicine <= index && stopMedicine === -1) {
                        tableRow += "<td class=\"text-center font-weight-bold\">";
                        tableRow += "<span class=\"text-success\">X</span>";
                        tableRow += "</td>";
                    } else if (startMedicine <= index && index < stopMedicine) {
                        tableRow += "<td class=\"text-center font-weight-bold\">";
                        tableRow += "<span class=\"text-success\">X</span>";
                        tableRow += "</td>";
                    } else if (stopMedicine === index) {
                        tableRow += "<td class=\"text-center font-weight-bold\">";
                        tableRow += "<span class=\"text-danger\">X</span>";
                        tableRow += "</td>";
                    } else {
                        tableRow += "<td></td>";
                    }
                }
            }

            tableRow += "<td>"
            tableRow += "<div class=\"form-group text-center\">";
            if (item.CurrentlyMedicine === "1") {
                tableRow += "<a href=\"#\" onclick=\"handleStopMedicine(" + item.Id + ")\" data-target=\"#myModal\" data-toggle=\"modal\" class=\"pop-modal btn btn-sm btn-outline-danger mr-2\">" + "@IRCEP.Resources.GlobalResource.StoppedTakeButton" + "</a>";
            }
            tableRow += "<button onclick=\"deleteMedicineRow(" + item.Id + ", " + item.FormSubmitId + ")\" class=\"btn btn-sm btn-danger\"" +
                "type =\"button\">" + "@IRCEP.Resources.GlobalResource.DeleteButton" + "</button>";
            tableRow += "</div>";
            tableRow += "</td>";

            tableRow += "</tr>";
        }

        tableRow += "</tbody>";
        return tableRow;
    }
}
            

function createHabitTableThead() {
    var thead = "<thead>";
    thead += "<tr>";
    thead += "<th class=\"text-center font-weight-bold\" width=\"\">" + "@IRCEP.Resources.GlobalResource.MedicationName" + "</th>";
    thead += "<th class=\"text-center font-weight-bold\" width=\"\">" + "@IRCEP.Resources.GlobalResource.Frequency" + "</th>";
    thead += "<th class=\"text-center font-weight-bold\" width=\"\">" + "@IRCEP.Resources.GlobalResource.BeforePregnancy" + "</th>";
    thead += "<th class=\"text-center font-weight-bold\" width=\"\">" + "@IRCEP.Resources.GlobalResource.FirstTrimester" + "</th>";
    thead += "<th class=\"text-center font-weight-bold\" width=\"\">" + "@IRCEP.Resources.GlobalResource.SecondTrimester" + "</th>";
    thead += "<th class=\"text-center font-weight-bold\" width=\"\">" + "@IRCEP.Resources.GlobalResource.ThirdTrimester" + "</th>";
    thead += "<th class=\"text-center font-weight-bold\" width=\"\">" + "@IRCEP.Resources.GlobalResource.AfterPregnancy" + "</th>";
    thead += "<th class=\"text-right font-weight-bold\" width=\"15%\">" + "@IRCEP.Resources.GlobalResource.Action" + "</th>";
    thead += "</tr>";
    thead += "</thead>";
    return thead;
}

function habitMedicines(medicine) {
    if (medicine.UserMedicines.length > 0) {
        $('#medicineTable').empty();
        var table = "";
        table = "<div class=\"text-left mb-4\">";
        table += "<h5>" + "@IRCEP.Resources.GlobalResource.MedicationHeaderTable" + "</h5>";
        table += "<div>";
        table += "<div class=\"table-responsive mt-4\">";
        table += "<table class=\"table table-bordered table-sm\">";
        table += createHabitTableThead();
        table += createHabitTableBody(medicine.UserMedicines)
        table += "</table>";
        table += "</div>";
        $('#medicineTable').append(table);
    } else {
        $('#medicineTable').empty();
    }
}

//function change_to_unbind(el) {
//    $(el).unbind("focus");
//    //rest of the change_my_profile code goes here
//}

function handleStopMedicine(Id) {
    $('#popUpId').val(Id);
}

function deleteMedicineRow(Id, FormSubmitId) {
    $.ajax({
        type: "POST",
        url: "/Medicines/DeleteRow",
        data: {
            __RequestVerificationToken: gettoken(),
            id: Id,
            formSubmitId: FormSubmitId,
            categoryId: $('#CategoryId').val(),
            language: document.getElementsByTagName("html")[0].getAttribute("lang")
        },
        success: function (resp) {
            if (resp.success) {
                habitMedicines(resp.medicine);
            }
        },
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8'
    });
}

$('#tookSaveBtn').click(function (evt) {
    evt.preventDefault();

    var startMedicine = parseInt($('#StartMedicine').val());
    var stopMedicine = parseInt($('#StopMedicine').val());
    if (startMedicine > stopMedicine) {
        Swal.fire({
            title: "<p><strong>" + "@IRCEP.Resources.GlobalResource.ErrorStopDate" + "</strong></p>",
            text: "",
            icon: 'error',
            confirmButtonColor: '#5bbc2e',
        })
        return false;
    }

    $.ajax({
        type: "POST",
        url: "/Medicines/StopMedicine",
        cache: false,
        data: {
            __RequestVerificationToken: gettoken(),
            id: $('#popUpId').val(),
            formSubmitId: $('#FormSubmitId').val(),
            categoryId: $('#CategoryId').val(),
            stopMedicine: $('#ModelStopMedicine').val(),
            language: document.getElementsByTagName("html")[0].getAttribute("lang")
        },
        success: function (resp) {
            $('#myModal').modal('hide');
            if (resp.success) {
                habitMedicines(resp.medicine);
                $("#messageToClient").val('');
            } else {
                $("#messageToClient").val(resp.messageToClient);
            }
        },
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8'
    });
});

$('#Search').autocomplete({
    minChars: 3,
    serviceUrl: '/Medicines/AutoComplete',
    onSearchStart: function (params) {
        params.id = this.id;
        params.categoryId = $('#CategoryId').val(),
            params.language = document.getElementsByTagName("html")[0].getAttribute("lang")
    },
    paramName: 'searchTerm',
    onSelect: function (suggestion) {
        if (suggestion.value !== '') {
            $('#searchForm').show();
        } else {
            $('#searchForm').hide();
            $('#StartMedicine').val('');
            $('#StopMedicine').val('');
            $('#CurrentlyMedicine').val('');
            $('#Drugs').val('');
            $('#Explain').val('');
            $('#drugChild').hide();
            $('#currentlyChild').hide();
            $("[name=CurrentlyMedicine]").prop('checked', false);
        }
    }
});

$('#Search').keyup(function (evt) {
    evt.preventDefault();
    var keycode = (evt.keyCode ? evt.keyCode : evt.which);
    if (keycode == '13') {
        var searchValue = $('#Search').val();
        if (searchValue !== '') {
            $('#searchForm').show();
        } else {
            $('#searchForm').hide();
            $('#StartMedicine').val('');
            $('#StopMedicine').val('');
            $('#CurrentlyMedicine').val('');
            $('#Drugs').val('');
            $('#Explain').val('');
            $('#drugChild').hide();
            $('#currentlyChild').hide();
            $("[name=CurrentlyMedicine]").prop('checked', false);
        }
    } else {
        var value = $(this).val();
        if (value === '') {
            $('#searchForm').hide();
            $('#StartMedicine').val('');
            $('#StopMedicine').val('');
            $('#CurrentlyMedicine').val('');
            $('#Drugs').val('');
            $('#Explain').val('');
            $('#drugChild').hide();
            $('#currentlyChild').hide();
            $("[name=CurrentlyMedicine]").prop('checked', false);
        }
    }
});

$('#searchBtn').click(function (evt) {
    evt.preventDefault();
    var searchValue = $('#Search').val();
    if (searchValue !== '') {
        $('#searchForm').show();
    } else {
        $('#searchForm').hide();
        $('#StartMedicine').val('');
        $('#StopMedicine').val('');
        $('#CurrentlyMedicine').val('');
        $('#Drugs').val('');
        $('#Explain').val('');
        $('#drugChild').hide();
        $('#currentlyChild').hide();
        $("[name=CurrentlyMedicine]").prop('checked', false);
    }
});

$('input[name="CurrentlyMedicine"]').on('change', function (evt) {
    evt.preventDefault();
    thisValue = $(this).val();
    if (thisValue === "1") {
        $('#currentlyChild').hide();
        $('#StopMedicne').val('');
    } else if (thisValue === "0") {
        $('#currentlyChild').show();
    }
});

$('#Drugs').on('change', function (evt) {
    evt.preventDefault();
    if ($(this).val() === "18") {
        $('#drugChild').show();
    } else {
        $('#drugChild').hide();
        $('#Explain').val('');
    }
});

$('#StartMedicine').on('change', function (evt) {
    evt.preventDefault();

    var startMedicine = parseInt($(this).val());
    if ($('#StopMedicine').val() !== "") {
        var stopMedicine = parseInt($('#StopMedicine').val());
        if (startMedicine > stopMedicine) {
            $('#StopMedicine').val('');
        }
    }
});

$('#StopMedicine').on('change', function (evt) {
    evt.preventDefault();
    var startMedicine = parseInt($('#StartMedicine').val());
    var stopMedicine = parseInt($(this).val());
    if (startMedicine > stopMedicine) {
        $(this).val('');
        Swal.fire({
            title: "<p><strong>" + "@IRCEP.Resources.GlobalResource.ErrorStopDate" + "</strong></p>",
            text: "",
            icon: 'error',
            confirmButtonColor: '#5bbc2e',
        })
    }
});

$('#medSubmitBtn').click(function (evt) {
    evt.preventDefault();
    $.ajax({
        type: "POST",
        url: "/Medicines/Submit",
        data: $('#medicineForm').serialize() + "&language=" + document.getElementsByTagName("html")[0].getAttribute("lang"),
        cache: false,
        success: function (resp) {
            if (resp.success) {
                $('#searchForm').hide();
                $('#StartMedicine').val('');
                $('#StopMedicine').val('');
                $('#CurrentlyMedicine').val('');
                $('#Drugs').val('');
                $('#Explain').val('');
                $('#Search').val('');
                $('#drugChild').hide();
                $("[name=CurrentlyMedicine]").prop('checked', false);
                $('#currentlyChild').hide();
                if (resp.success) {
                    habitMedicines(resp.medicine);
                }
            }
        }
    });
});