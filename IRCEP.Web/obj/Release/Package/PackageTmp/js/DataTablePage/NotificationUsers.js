﻿$(document).ready(function () {
    $.noConflict();
    $('#customTable').DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "ordering": true,
        "order": [[6, "desc"]],
        "lengthMenu": [25, 50, 100, 150],
        "ajax": {
            "dataType": 'json',
            "type": "POST",
            "url": '/ApplicationUsers/NotifiedUsers',
            "dataSrc": function (json) {
                $('#totalUsers').text(json.data.TotalCount);
                return json.data.Items;
            }
        },
        columns: [
            { data: "FirstName" },
            { data: "UserNumber" },
            { data: "RegisterCountry" },
            { data: "PhoneNumber" },
            {
                data: "PhoneNumberConfirmed",
                bSortable: false,
                render: function (data, type, row, meta) {
                    if (row.PhoneNumberConfirmed) {
                        return '<span class="badge badge-success">Verified</span>';
                    } else {
                        return '<span class="badge badge-danger">Unverified</span>';
                    }
                }
            },
            {
                data: "ConsentSign",
                bSortable: false,
                render: function (data, type, row, meta) {
                    if (row.ConsentSign) {
                        return '<span class="badge badge-success">Verified</span>';
                    } else {
                        return '<span class="badge badge-danger">Unverified</span>';
                    }
                }
            },
            {
                data: "CreatedDateToString",
                render: function (data, type, row, meta) {
                    return data;
                }
            },
            {
                'data': 'Action', "bSortable": false, 'render': function (data, type, row, meta) {
                    var UserId = row.Id;
                    var link = "<button type=\"button\" class=\"btn btn-sm btn-primary\" onclick=\"handleStop('" + UserId + "')\">Stop Notification</button>";
                    return link;
                }
            }
        ]
    })
});




