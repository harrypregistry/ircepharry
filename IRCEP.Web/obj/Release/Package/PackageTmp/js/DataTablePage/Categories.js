﻿$(document).ready(function () {
    $.noConflict();
    $('#customTable').DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "ordering": true,
        "order": [[1, "desc"]],
        "lengthMenu": [25, 50, 100, 150],
        "pageLength": 25,
        "ajax": {
            "dataType": 'json',
            "type": "POST",
            "url": '/Categories/IndexJson',
            "dataSrc": function (json) {
                return json.data.Items;
            }
        },
        columns: [
            {
                data: "CategoryName",
                render: function (data, type, row, meta) {
                    var data = "";
                    data += "<div class=\"text-left\">";
                    data += row.CategoryName;
                    data += "</div>";
                    return data;
                }
            },
            {
                data: "Role",
                render: function (data, type, row, meta) {
                    var data = "";
                    data += "<div class=\"text-left\">";
                    if (row.Role === "Pregnant") {
                        data += "Pregnant";
                    } else if (row.Role === "NonPregnant") {
                        data += "Non Pregnant";
                    }
                    data += "</div>";
                    return data;
                }
            },
            {
                data: "Sequence",
                render: function (data, type, row, meta) {
                    var data = "";
                    data += "<div class=\"text-center\">";
                    data += row.Sequence;
                    data += "</div>";
                    return data;
                }
            },
            {
                'data': 'Action', "bSortable": false, 'render': function (data, type, row, meta) {
                    var id = row.Id;

                    var createQuestionAnchorLink = "/Questions/Create?id=" + id;
                    var questionAnchorLink = "/Categories/Questions?id=" + id;
                    var editAnchorLink = "/Categories/Edit?id=" + id;
                    var deleteAnchorLink = "/Categories/Delete?id=" + id;

                    var link = "";
                    link += "<div class=\"text-right\">";
                    link += "<div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"Third group\">" +
                        "<a href=\"" + createQuestionAnchorLink + "\" class=\"btn btn-sm btn-success\">" + 
                        "<span class=\"fa fa-plus\"></span>" + 
                        "</a>" + 
                        "</div> ";
                    
                    link += "<div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"Third group\">" +
                        "<a href=\"" + questionAnchorLink + "\" class=\"btn btn-sm btn-success\">" +
                        "<span class=\"fa fa-info\"></span>" +
                        "</a>" +
                        "</div> ";

                    link += "<div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"Third group\">" +
                        "<a href=\"" + editAnchorLink + "\" class=\"btn btn-sm btn-primary\">" +
                        "<span class=\"fa fa-edit\"></span>" +
                        "</a>" +
                        "</div> ";

                    link += "<div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"Third group\">" +
                        "<a href=\"" + deleteAnchorLink + "\" class=\"btn btn-sm btn-danger\">" +
                        "<span class=\"fa fa-trash\"></span>" +
                        "</a>" +
                        "</div>";
                    link += "</div>";

                    return link;
                }
            }
        ]
    })
});




