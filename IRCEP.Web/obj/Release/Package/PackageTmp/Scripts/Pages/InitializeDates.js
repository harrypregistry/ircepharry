﻿function dateComparison(startDate, endDate) {
    var isAfter = moment(startDate).isAfter(endDate);
    if (isAfter) {
        return false;
    }
    return true;
}

function initDateObject(questionId, lmpDateTime, todayDateTime, dateOfBirth) {
    const lmpDate = moment(lmpDateTime);
    const todayDate = moment(todayDateTime);
    var dataObj = {
        startDate: moment().subtract(12, 'months').format('MM/DD/YYYY'),
        endDate: todayDate.format('MM/DD/YYYY'),
        success: true,
        messageToClient: ""
    };
    if (questionId === "1558" || questionId === "1562") {
        if (dateOfBirth !== "") {
            var dateOfBirthStart = moment(dateOfBirth);
            var dateOfBirthEnd = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(30, 'days').format('MM/DD/YYYY');
            dataObj.endDate = dateOfBirthEnd.add(90, 'days').format('MM/DD/YYYY');
            return dataObj;
        }
    } else if (questionId === "1560") {
        var dateOfBirthStart = moment(dateOfBirth);
        var dateOfBirthEnd = moment(dateOfBirth);
        dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
        dataObj.endDate = dateOfBirthEnd.format('MM/DD/YYYY');
    } else if (questionId === "2228" || questionId === "2109" ||
        questionId === "2114" || questionId === "2122" || questionId === "2117" || questionId === "2110" || questionId === "2113" ||
        questionId === "2121" || questionId === "2118") {
        dataObj.startDate = moment(dateOfBirth).format('MM/DD/YYYY');
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
    } else if (questionId === "2230" || questionId === "2232" || questionId === "2234" || questionId === "2237" || questionId === "2243" ||
        questionId === "2245" || questionId === "2247" || questionId === "2249") {
        dataObj.startDate = moment(dateOfBirth).format('MM/DD/YYYY');
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
    } else if (questionId === "2159" || questionId === "2159" || questionId === "2689" || questionId === "2844" || questionId === "2292" || questionId === "2703" ||
        questionId === "2700" || questionId === "2712" || questionId === "2720" || questionId === "2744" || questionId === "2748" || questionId === "2723" ||
        questionId === "2820" || questionId === "2828" || questionId === "2828" || questionId === "2836" || questionId === "2836" || questionId === "2751" ||
        questionId === "2768" || questionId === "2821" || questionId === "2829" || questionId === "2837" || questionId === "2845" || questionId === "2824" ||
        questionId === "2688" || questionId === "2690" || questionId === "2691" || questionId === "2701" || questionId === "2702" || questionId === "2832" ||
        questionId === "2713" || questionId === "2714" || questionId === "2715" || questionId === "2721" || questionId === "2722" ||
        questionId === "2745" || questionId === "2746" || questionId === "2747" || questionId === "2749" || questionId === "2750" ||
        questionId === "2769" || questionId === "2770" || questionId === "2771" || questionId === "2822" || questionId === "2823" ||
        questionId === "2825" || questionId === "2826" || questionId === "2827" || questionId === "2830" || questionId === "2831" ||
        questionId === "2833" || questionId === "2834" || questionId === "2835" || questionId === "2838" || questionId === "2839" || questionId === "2840" ||
        questionId === "2841" || questionId === "2842" || questionId === "2843" || questionId === "2846" || questionId === "2847" || questionId === "2848" ||
        questionId === "2849" || questionId === "2850" || questionId === "2851" || questionId === "1590" || questionId === "1591" || questionId === "1598" ||
        questionId === "1599" || questionId === "1961") {
        if (dateOfBirth != "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
            return dataObj;
        }
    } else if (questionId === "2409" || questionId === "2416" || questionId === "2433" || questionId === "2437") {
        if (dateOfBirth != "") {
            dataObj.startDate = moment(dateOfBirth).subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = moment(dateOfBirth).format('MM/DD/YYYY');
            return dataObj;
        }
    } else if (questionId === "1657") {
        var value = document.getElementById("1658").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    } else if (questionId === "1624") {
        var value = document.getElementById("1625").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    } else if (questionId === "1625") {
        var value = document.getElementById("1624").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "2173") {
        var value = document.getElementById("2174").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    } else if (questionId === "2174") {
        var value = document.getElementById("2173").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1605") {
        var value = document.getElementById("1960").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    } else if (questionId === "1960") {
        var value = document.getElementById("1605").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }

    else if (questionId === "1618") {
        var value = document.getElementById("1619").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    } else if (questionId === "1619") {
        var value = document.getElementById("1618").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1616") {
        var value = document.getElementById("1617").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    } else if (questionId === "1617") {
        var value = document.getElementById("1616").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "2192") {
        var value = document.getElementById("2194").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    } else if (questionId === "2194") {
        var value = document.getElementById("2192").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1626") {
        var value = document.getElementById("1627").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1627") {
        var value = document.getElementById("1626").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "1628") {
        var value = document.getElementById("1629").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1629") {
        var value = document.getElementById("1628").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1630") {
        var value = document.getElementById("1633").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1633") {
        var value = document.getElementById("1630").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "1634") {
        var value = document.getElementById("1635").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1635") {
        var value = document.getElementById("1634").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1631") {
        var value = document.getElementById("1632").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1632") {
        var value = document.getElementById("1631").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1636") {
        var value = document.getElementById("1637").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1637") {
        var value = document.getElementById("1636").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "1638") {
        var value = document.getElementById("1639").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1639") {
        var value = document.getElementById("1638").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1640") {
        var value = document.getElementById("1641").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1641") {
        var value = document.getElementById("1640").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1642") {
        var value = document.getElementById("1643").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1643") {
        var value = document.getElementById("1642").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1644") {
        var value = document.getElementById("1645").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1645") {
        var value = document.getElementById("1644").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1646") {
        var value = document.getElementById("1647").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1647") {
        var value = document.getElementById("1646").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1961") {
        var value = document.getElementById("1649").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1649") {
        var value = document.getElementById("1961").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1650") {
        var value = document.getElementById("1651").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1651") {
        var value = document.getElementById("1650").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "2197") {
        var value = document.getElementById("2199").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "2199") {
        var value = document.getElementById("2197").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "2193") {
        var value = document.getElementById("2195").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "2195") {
        var value = document.getElementById("2193").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1652") {
        var value = document.getElementById("1653").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1653") {
        var value = document.getElementById("1652").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1654") {
        var value = document.getElementById("1655").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "1655") {
        var value = document.getElementById("1654").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "2196") {
        var value = document.getElementById("2198").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "2198") {
        var value = document.getElementById("2196").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    }
    else if (questionId === "1653") {
        var value = document.getElementById("1952").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "1655") {
        var value = document.getElementById("1954").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "1658") {
        var value = document.getElementById("1657").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "2288") {
        var value = document.getElementById("2289").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "2289") {
        var value = document.getElementById("2288").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "2291") {
        var value = document.getElementById("2292").value;
        if (value === "") {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = todayDate.format('MM/DD/YYYY');
        } else {
            var dateOfBirthStart = moment(dateOfBirth);
            dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
            dataObj.endDate = value;
        }
        return dataObj;
    }
    else if (questionId === "2292") {
        var value = document.getElementById("2291").value;
        if (value === "") {
            dataObj.success = false;
            dataObj.messageToClient = "Please enter the start date";
            return dataObj;
        }
        var isValid = dateComparison(moment(value, 'MM/DD/YYYY'), moment(todayDateTime, 'MM/DD/YYYY'));
        if (!isValid) {
            dataObj.success = false;
            dataObj.messageToClient = "Start date is greater than stop date";
            return dataObj;
        }
        dataObj.startDate = value;
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "2162" || questionId === "2165" || questionId === "2167" || questionId === "2179") {
        var dateOfBirthStart = moment(dateOfBirth);
        dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
    } else if (questionId === "2139" || questionId === "2251" || questionId === "2252" || questionId === "2253" || questionId === "2254") {
        var dateOfBirthStart = moment(dateOfBirth);
        dataObj.startDate = dateOfBirthStart.format('MM/DD/YYYY');
        dataObj.endDate = dateOfBirthStart.format('MM/DD/YYYY');
        return dataObj;
    } else if (questionId === "2162" || questionId === "2165" || questionId === "2167" || questionId === "2173" || questionId === "2179") {
        var dateOfBirthStart = moment(dateOfBirth);
        dataObj.startDate = dateOfBirthStart.subtract(280, 'days').format('MM/DD/YYYY');
        dataObj.endDate = todayDate.format('MM/DD/YYYY');
    } else if (questionId === "2281" || questionId === "2283" || questionId === "2286" || questionId === "2297" || questionId === "2299") {
        dataObj.startDate = moment().subtract(365, 'days').format('MM/DD/YYYY');
    } else if (questionId === "2282") {
        const startDateValue = document.getElementById('2281').value;
        if (startDateValue !== "") {
            dataObj.startDate = startDateValue;
        }
    } else if (questionId === "2284") {
        const startDateValue = document.getElementById('2283').value;
        if (startDateValue !== "") {
            dataObj.startDate = startDateValue;
        }
    } else if (questionId === "2287") {
        const startDateValue = document.getElementById('2286').value;
        if (startDateValue !== "") {
            dataObj.startDate = startDateValue;
        }
    } else if (questionId === "2298") {
        const startDateValue = document.getElementById('2297').value;
        if (startDateValue !== "") {
            dataObj.startDate = startDateValue;
        }
    } else if (questionId === "2300") {
        const startDateValue = document.getElementById('2299').value;
        if (startDateValue !== "") {
            dataObj.startDate = startDateValue;
        }
    } else if (questionId === "1533" || questionId === "1556") {
        dataObj.startDate = moment().subtract(365, 'days').format('MM/DD/YYYY');
    } else if (questionId === "136" || questionId === "26" || questionId === "30") {
        var addedLampDate = lmpDate.add(280, 'days');
        var newLampDateStart = addedLampDate;
        var newLampDateEnd = addedLampDate;
        dataObj.startDate = newLampDateStart.subtract(90, 'days').format('MM/DD/YYYY');
        dataObj.endDate = newLampDateEnd.add(120, 'days').format('MM/DD/YYYY');
    } else if (questionId === "889" || questionId === "887" || questionId === "922" || questionId === "918" || questionId === "2027" || questionId === "74" || questionId === "77" || questionId === "1009" || questionId === "1010" || questionId === "86" || questionId === "511" || questionId === "513" || questionId === "515" || questionId === "522" || questionId === "517" || questionId === "519" || questionId === "524" || questionId === "526" || questionId === "528" || questionId === "530" || questionId === "532" || questionId === "534" || questionId === "536" || questionId === "538" || questionId === "540" || questionId === "542" || questionId === "545") {
        dataObj.startDate = moment().subtract(365, 'days').format('MM/DD/YYYY');
    } else if (questionId === "890") {
        var startCovidDateEle = document.getElementById("889");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "888") {
        var startCovidDateEle = document.getElementById("887");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "925") {
        var startCovidDateEle = document.getElementById("922");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "919") {
        var startCovidDateEle = document.getElementById("918");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "90") {
        var startCovidDateEle = document.getElementById("86");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "512") {
        var startCovidDateEle = document.getElementById("511");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "514") {
        var startCovidDateEle = document.getElementById("513");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "516") {
        var startCovidDateEle = document.getElementById("515");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "521") {
        var startCovidDateEle = document.getElementById("517");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "523") {
        var startCovidDateEle = document.getElementById("522");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "520") {
        var startCovidDateEle = document.getElementById("519");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "525") {
        var startCovidDateEle = document.getElementById("524");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "527") {
        var startCovidDateEle = document.getElementById("526");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "529") {
        var startCovidDateEle = document.getElementById("528");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "531") {
        var startCovidDateEle = document.getElementById("530");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "533") {
        var startCovidDateEle = document.getElementById("532");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "535") {
        var startCovidDateEle = document.getElementById("534");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "537") {
        var startCovidDateEle = document.getElementById("536");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "539") {
        var startCovidDateEle = document.getElementById("538");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "541") {
        var startCovidDateEle = document.getElementById("540");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "543") {
        var startCovidDateEle = document.getElementById("542");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "546") {
        var startCovidDateEle = document.getElementById("545");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "1798" || questionId === "1800") {
        dataObj.startDate = moment().subtract(365, 'days').format('MM/DD/YYYY');
    } else if (questionId === "1799") {
        var startCovidDateEle = document.getElementById("1798");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "1801") {
        var startCovidDateEle = document.getElementById("1800");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "845" || questionId === "852" || questionId === "1147" || questionId === "1477" || questionId === "1478" || questionId === "861" || questionId === "873" || questionId === "875" || questionId === "839" || questionId === "844" || questionId === "1471" || questionId === "1472" || questionId === "880") {
        dataObj.startDate = moment().subtract(30, 'days').format('MM/DD/YYYY');
    } else if (questionId === "24" || questionId === "28" || questionId === "74" || questionId === "77" || questionId === "86" || questionId === "1009" || questionId === "1010" || questionId === "136") {
        dataObj.startDate = lmpDate.format('MM/DD/YYYY');
    } else if (questionId === "511" || questionId === "512" || questionId === "513" || questionId === "514" || questionId === "515" || questionId === "516" || questionId === "517" || questionId === "518" || questionId === "522" || questionId === "523" || questionId === "519" || questionId === "520" || questionId === "524" || questionId === "525" || questionId === "526" || questionId === "527" || questionId === "528" || questionId === "529" || questionId === "530" || questionId === "531" || questionId === "532" || questionId === "533" || questionId === "534" || questionId === "535" || questionId === "536" || questionId === "537" || questionId === "538" || questionId === "539" || questionId === "540" || questionId === "541" || questionId === "542" || questionId === "543" || questionId === "545" || questionId === "546" || questionId === "835" || questionId === "837") {
        dataObj.startDate = lmpDate.format('MM/DD/YYYY');
    } else if (questionId === "836") {
        var startCovidDateEle = document.getElementById("835");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "838") {
        var startCovidDateEle = document.getElementById("837");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "862") {
        var startCovidDateEle = document.getElementById("861");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "90") {
        var startCovidDateEle = document.getElementById("86");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "876") {
        var startCovidDateEle = document.getElementById("875");
        if (startCovidDateEle.value !== "") {
            var startCovidDate = startCovidDateEle.value.split('/');
            var covidDate = moment(startCovidDate[2] + "-" + startCovidDate[0] + "-" + startCovidDate[1]);
            dataObj.startDate = covidDate.format('MM/DD/YYYY');
        }
    } else if (questionId === "226" || questionId === "1124") {
        dataObj.startDate = lmpDate.format('MM/DD/YYYY');
    }
    return dataObj;
} 