﻿$(document).ready(function () {
    $.noConflict();

    $('#tblFeedback').DataTable({
        dom: 'lBfrtip',
        "ajax": {
            "dataType": 'json',
            "type": "GET",
            "url": '/ApplicationUsers/GetFeedback',
            "dataSrc": function (json) {

                return json;
            }
        },
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        pageLength: 10,
        sPaginationType: "full_numbers",
        buttons: [
            {
                extend: 'excel',
                text: 'Download'
            },           
        ]
        ,
        columns: [
            { data: "UserNumber" },
            { data: "CategoryName" },
            { data: "Text" },
            { data: "CreatedDate" }
        ]
    })
});


