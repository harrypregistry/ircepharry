﻿function createMonthlyUpdateThead(medicine) {
    var colSpanLength = medicine.Months.length + 1;
    var thead = "<thead>";
    thead += "<tr>";
    thead += "<th colspan=\"2\"></th>";
    thead += "<th colspan=\"" + colSpanLength + "\" class=\"text-left font-weight-bold\" scope=\"col\">" + "@IRCEP.Resources.GlobalResource.LMP" + "</th>";
    thead += "</tr>";
    thead += "</thead>";
    return thead;
}

function createMonthlyUpdateTableBody(medicine) {
    if (medicine.UserMedicines.length > 0) {
        var tbody = "<tbody>";
        tbody += "<tr>";
        tbody += "<td class=\"text-center font-weight-bold\" width=\"20%\">" + "@IRCEP.Resources.GlobalResource.MedicationName" + "</td>";
        tbody += "<td class=\"text-center font-weight-bold\" width=\"20%\">" + "@IRCEP.Resources.GlobalResource.Frequency" + "</td>";
        if (medicine.Months.length > 0) {
            for (var index = 0; index < medicine.Months.length; index++) {
                var serailNo = medicine.Months[index].SerialNo;
                tbody += "<td class=\"text-center font-weight-bold\">" + serailNo + "</td>";
            }
        }
        tbody += "<td width=\"18%\" class=\"text-right font-weight-bold\">" + "@IRCEP.Resources.GlobalResource.Action" + "</td>";
        tbody += "</tr>";
        for (var i = 0; i < medicine.UserMedicines.length; i++) {
            var insertedValue = false;
            var item = medicine.UserMedicines[i];
            tbody += "<tr>";
            tbody += "<td class=\"text-center\">" + item.Search + "</td>";
            tbody += "<td class=\"text-center\">" + item.Drugs + "</td>";
            if (medicine.Months.length > 0) {
                for (var index = 0; index < medicine.Months.length; index++) {
                    var monthId = medicine.Months[index].MonthId;
                    tbody += "<td class=\"text-center font-weight-bold\">";
                    if (item.StartMedicineDate !== "") {
                        if (monthId == parseInt(item.StartMedicineDate.split('/')[0])) {
                            insertedValue = true;
                            tbody += "<span class=\"text-success\">X </span>";
                        }
                    }
                    if (item.StopMedicineDate !== "") {
                        if (monthId == parseInt(item.StopMedicineDate.split('/')[0])) {
                            insertedValue = false;
                            tbody += "<span class=\"text-danger\">X</span>";
                        }
                    }
                    if (item.StartMedicineDate !== "" && item.StopMedicineDate !== "") {
                        if (monthId !== parseInt(item.StartMedicineDate.split('/')[0]) && monthId !== parseInt(item.StopMedicineDate.split('/')[0])) {
                            if (insertedValue) {
                                tbody += "<span class=\"text-success\">X </span>";
                            }
                        }
                    } else {
                        if (monthId !== parseInt(item.StartMedicineDate.split('/')[0])) {
                            if (insertedValue) {
                                tbody += "<span class=\"text-success\">X </span>";
                            }
                        }
                    }
                    tbody += "</td>";
                }
            }
            tbody += "<td>";
            tbody += "<div class=\"form-group text-right\">";
            if (item.CurrentlyMedicine === "1") {
                tbody += "<a href=\"#\" onclick=\"handleChangeOption(" + item.Id + ",'" + item.StartMedicineDate + "')\" data-target=\"#myModal\" data-toggle=\"modal\" class=\"pop-modal btn btn-sm btn-outline-danger mr-2\">" + "@IRCEP.Resources.GlobalResource.StoppedTakeButton" + "</a>";
            }
            tbody += "<button onclick=\"deleteTable(" + item.Id + ", " + item.FormSubmitId + ")\" class=\"btn btn-sm btn-danger mt-1\"" +
                "type =\"button\">" + "@IRCEP.Resources.GlobalResource.DeleteButton" + "</button>";
            tbody += "</div>";
            tbody += "</td>";
            tbody += "</tr>";
        }
        tbody += "</tbody>";
        return tbody;
    }
}

function monthlyUpdateMedicine(medicine) {
    if (medicine.UserMedicines.length > 0) {
        $('#medicineTable').empty();
        var table = '';
        table += "<div class=\"text-left mb-4\">";
        table += "<h5>" + "@IRCEP.Resources.GlobalResource.MedicationHeaderTable" + "</h5>";
        table += "<table class=\"table table-bordered table-sm\">";
        table += createMonthlyUpdateThead(medicine);
        table += createMonthlyUpdateTableBody(medicine);
        table += "</table>";
        table += "</div>";
        $('#medicineTable').append(table);
    } else {
        $('#medicineTable').empty();
    }
}

function deleteTable(Id, FormSubmitId) {
    $.ajax({
        type: "POST",
        url: "/Medicines/Delete",
        data: {
            __RequestVerificationToken: gettoken(),
            id: Id,
            formSubmitId: FormSubmitId,
            categoryId: $('#CategoryId').val(),
            language: document.getElementsByTagName("html")[0].getAttribute("lang")
        },
        success: function (resp) {
            if (resp.success) {
                monthlyUpdateMedicine(resp.medicine);
            }
        },
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8'
    });
}

function handleChangeOption(Id, startDateValue) {
    $('#popUpId').val(Id);
    document.getElementById('startTookDate').value = startDateValue;
}



    (function ($) {
        $('#Search').autocomplete({
            minChars: 3,
            serviceUrl: '/Medicines/AutoComplete',
            onSearchStart: function (params) {
                params.id = this.id;
                params.categoryId = $('#CategoryId').val(),
                    params.language = document.getElementsByTagName("html")[0].getAttribute("lang")
            },
            paramName: 'searchTerm',
            onSelect: function (suggestion) {
                if (suggestion.value !== '') {
                    $('#searchForm').show();
                } else {
                    $('#searchForm').hide();
                    $('#StartMedicine').val('');
                    $('#StopMedicine').val('');
                    $('#CurrentlyMedicine').val('');
                    $('#Drugs').val('');
                    $('#Explain').val('');
                    $('#drugChild').hide();
                    $('#currentlyChild').hide();
                    $("[name=CurrentlyMedicine]").prop('checked', false);
                }
            }
        });

                $('#Search').keyup(function (evt) {
        evt.preventDefault();
                    var keycode = (evt.keyCode ? evt.keyCode : evt.which);
                    if (keycode == '13') {
                        var searchValue = $('#Search').val();
                        if (searchValue !== '') {
        $('#searchForm').show();
                        } else {
        $('#searchForm').hide();
                            $('#StartMedicine').val('');
                            $('#StopMedicine').val('');
                            $('#CurrentlyMedicine').val('');
                            $('#Drugs').val('');
                            $('#Explain').val('');
                            $('#drugChild').hide();
                            $('#currentlyChild').hide();
                            $("[name=CurrentlyMedicine]").prop('checked', false);
                        }
                    } else {
                        var value = $(this).val();
                        if (value === '') {
        $('#searchForm').hide();
                            $('#StartMedicine').val('');
                            $('#StopMedicine').val('');
                            $('#CurrentlyMedicine').val('');
                            $('#Drugs').val('');
                            $('#Explain').val('');
                            $('#drugChild').hide();
                            $('#currentlyChild').hide();
                            $("[name=CurrentlyMedicine]").prop('checked', false);
                        }
                    }
                });

                $('#searchBtn').click(function (evt) {
        evt.preventDefault();
                    var searchValue = $('#Search').val();
                    if (searchValue !== '') {
        $('#searchForm').show();
                    } else {
        $('#searchForm').hide();
                        $('#StartMedicine').val('');
                        $('#StopMedicine').val('');
                        $('#CurrentlyMedicine').val('');
                        $('#Drugs').val('');
                        $('#Explain').val('');
                        $('#drugChild').hide();
                        $('#currentlyChild').hide();
                        $("[name=CurrentlyMedicine]").prop('checked', false);
                    }
                });

                $("#medicineForm").on('focus', '.datepicker', function () {

                    var StartDate = moment("@Model.LmpDate").format('MM/DD/YYYY');
                    var EndDate = moment("@Model.TodayDate").format('MM/DD/YYYY');


                    if ($(this).attr('id') === 'StopMedicineDate') {
                        if (document.getElementById('StartMedicineDate').value === '') {
        alert('Invalid date');
                            return false;
                        }
                        StartDate = document.getElementById('StartMedicineDate').value;
                    }

                    $(this).daterangepicker({
        singleDatePicker: true,
                        showDropdowns: true,
                        autoUpdateInput: true,
                        autoApply: true,
                        minDate: StartDate,
                        maxDate: EndDate,
                        locale: {
        format: 'MM/DD/YYYY',
                            cancelLabel: 'Clear',
                            monthNames: ['@IRCEP.Resources.GlobalResource.SelectJan',
                        '@IRCEP.Resources.GlobalResource.SelectFeb',
                        '@IRCEP.Resources.GlobalResource.SelectMar',
                        '@IRCEP.Resources.GlobalResource.SelectApr',
                        '@IRCEP.Resources.GlobalResource.SelectMay',
                        '@IRCEP.Resources.GlobalResource.SelectJun',
                        '@IRCEP.Resources.GlobalResource.SelectJul',
                        '@IRCEP.Resources.GlobalResource.SelectAug',
                        '@IRCEP.Resources.GlobalResource.SelectSep',
                        '@IRCEP.Resources.GlobalResource.SelectOct',
                        '@IRCEP.Resources.GlobalResource.SelectNov',
                        '@IRCEP.Resources.GlobalResource.SelectDec']
                        }
                    });
                });

                $("#main_contact_forms").on('focus', '.datepicker', function () {
                    var StartDate = document.getElementById('startTookDate').value;
                    var EndDate = moment("@Model.TodayDate").format('MM/DD/YYYY');


                    $(this).daterangepicker({
        singleDatePicker: true,
                        showDropdowns: true,
                        autoUpdateInput: true,
                        autoApply: true,
                        minDate: StartDate,
                        maxDate: EndDate,
                        locale: {
        format: 'MM/DD/YYYY',
                            cancelLabel: 'Clear',
                            monthNames: ['@IRCEP.Resources.GlobalResource.SelectJan',
                        '@IRCEP.Resources.GlobalResource.SelectFeb',
                        '@IRCEP.Resources.GlobalResource.SelectMar',
                        '@IRCEP.Resources.GlobalResource.SelectApr',
                        '@IRCEP.Resources.GlobalResource.SelectMay',
                        '@IRCEP.Resources.GlobalResource.SelectJun',
                        '@IRCEP.Resources.GlobalResource.SelectJul',
                        '@IRCEP.Resources.GlobalResource.SelectAug',
                        '@IRCEP.Resources.GlobalResource.SelectSep',
                        '@IRCEP.Resources.GlobalResource.SelectOct',
                        '@IRCEP.Resources.GlobalResource.SelectNov',
                        '@IRCEP.Resources.GlobalResource.SelectDec']
                        }
                    });
                });

                $('input[name="CurrentlyMedicine"]').on('change', function (evt) {
        evt.preventDefault();
                    thisValue = $(this).val();
                    if (thisValue === "1") {
        $('#currentlyChild').hide();
                        $('#StopMedicne').val('');
                    } else if (thisValue === "0") {
        $('#currentlyChild').show();
                    }
                });

                $('#Drugs').on('change', function (evt) {
        evt.preventDefault();
                    if ($(this).val() === "18") {
        $('#drugChild').show();
                    } else {
        $('#drugChild').hide();
                        $('#Explain').val('');
                    }
                });

                $('#tookBtn').click(function (evt) {
        evt.preventDefault();
                    $.ajax({
        type: "POST",
                        url: "/Medicines/ChangeOption",
                        cache: false,
                        data: {
        __RequestVerificationToken: gettoken(),
                            id: $('#popUpId').val(),
                            formSubmitId: $('#FormSubmitId').val(),
                            categoryId: $('#CategoryId').val(),
                            tookDate: $('#TookDate').val(),
                            language: document.getElementsByTagName("html")[0].getAttribute("lang")
                        },
                        success: function (resp) {
        $('#myModal').modal('hide');
                            if (resp.success) {
        $("#messageToClient").val('');
                                monthlyUpdateMedicine(resp.medicine);
                            } else {
        $("#messageToClient").val(resp.messageToClient);
                            }
                        },
                        dataType: 'json',
                        contentType: 'application/x-www-form-urlencoded; charset=utf-8'
                    });
                });

                $('#medSubmitBtn').click(function (evt) {
        evt.preventDefault();
                    $.ajax({
        type: "POST",
                        url: "/Medicines/Submit",
                        data: $('#medicineForm').serialize() + "&language=" + document.getElementsByTagName("html")[0].getAttribute("lang"),
                        cache: false,
                        success: function (resp) {
                            if (resp.success) {
        $('#searchForm').hide();
                                $('#StartMedicine').val('');
                                $('#StopMedicine').val('');
                                $('#CurrentlyMedicine').val('');
                                $('#Drugs').val('');
                                $('#Explain').val('');
                                $('#Search').val('');
                                $('#drugChild').hide();
                                $("[name=CurrentlyMedicine]").prop('checked', false);
                                $('#currentlyChild').hide();
                                monthlyUpdateMedicine(resp.medicine);
                            }
                        }
                    });
                });

            })(jQuery);

     