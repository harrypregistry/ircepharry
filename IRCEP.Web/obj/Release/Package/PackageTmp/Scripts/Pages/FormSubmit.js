﻿var formSubmitForm = document.getElementById("formSubmit");
formSubmitForm.addEventListener("submit", function (evt) {
    evt.preventDefault();

    var isRequired = true;
    //Thanks for your responses.At this time, you're not eligible to continue. Come back again if you get tested for COVID during your pregnancy. 
    //If you made a mistake in your responses, click here to go back.Otherwise, click here for more resources about COVID in pregnancy.
    var categoryId = document.getElementById("CategoryId").value;

    if (categoryId === "8" || categoryId === "37") {
        if (categoryId === "37") {
            if (($('#11800').prop('checked') === true && $('#19652').prop('checked') === true) || ($('#11800').prop('checked') === true && $('#19654').prop('checked') === true)) {
                isRequired = false;
                formSubmitForm.submit();
            }
        } else if (categoryId === "8") {
            if (($('#260').prop('checked') === true && $('#19651').prop('checked') === true) || ($('#260').prop('checked') === true && $('#19653').prop('checked') === true)) {
                isRequired = false;
                formSubmitForm.submit();
            }
        }
    }

    if (isRequired) {
        var isSubmit = messageToClient();
        if (isSubmit) {
            Swal.fire({
                title: endStudyMessage(),
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5BBC2E',
                cancelButtonColor: '#d33',
                confirmButtonText: yesMessage(),
                cancelButtonText: noMessage()
            }).then((result) => {
                if (result.value) {
                    document.getElementById("formSubmit").submit();
                    return false;
                }
            })
        } else {
            var isError = false;
            const inputs = document.getElementsByTagName("input");
            const selects = document.getElementsByTagName("select");
            var elementId = "";

            for (var i in inputs) if (inputs.hasOwnProperty(i)) {
                const required = inputs[i].getAttribute("data-required");
                const quesId = inputs[i].getAttribute("data-questionId");
                if (required !== null || required !== "") {
                    if (required === "true") {
                        const type = inputs[i].getAttribute("type");
                        if (type === "text") {
                            const value = inputs[i].value;
                            if (value === "" || value === null) {
                                if (inputs[i].classList.length > 0) {
                                    if (inputs[i].classList.contains("is-valid")) {
                                        inputs[i].classList.remove("is-valid");
                                    }
                                    if (!inputs[i].classList.contains("is-invalid")) {
                                        inputs[i].classList.add("is-invalid");
                                    }
                                } else {
                                    inputs[i].classList.add("is-invalid");
                                }
                                isError = true;
                                elementId = inputs[i].getAttribute("id");
                            }
                        } else if (type === "radio" || type === "checkbox") {
                            const divName = inputs[i].getAttribute('data-divName');
                            const name = inputs[i].getAttribute('name');
                            var groupName = document.getElementsByName(name);
                            var anyChecked = false;
                            for (i = 0; i < groupName.length; i++) {
                                if (groupName[i].checked) {
                                    anyChecked = true;
                                }
                            }
                            if (!anyChecked) {
                                isError = true;
                                elementId = inputs[i].getAttribute("id");
                                var parentEle = document.getElementById(divName + quesId + "-div");
                                if (parentEle.classList.length > 0) {
                                    if (parentEle.classList.contains("custom-val-color-g")) {
                                        parentEle.classList.remove("custom-val-color-g");
                                    }
                                    if (!parentEle.classList.contains("custom-val-color-r")) {
                                        parentEle.classList.add("custom-val-color-r");
                                    }
                                } else {
                                    parentEle.classList.add("custom-val-color-r");
                                }
                            }
                        }
                    }
                }
            }

            for (var i in selects) if (selects.hasOwnProperty(i)) {
                const required = selects[i].getAttribute("data-required");
                if (required !== null || required !== "") {
                    if (required === "true") {
                        var selectValue = selects[i].value;
                        if (selectValue === "") {
                            isError = true;
                            elementId = selects[i].getAttribute("id");
                            if (selects[i].classList.contains("is-valid")) {
                                selects[i].classList.remove("is-valid");
                            }
                            if (!selects[i].classList.contains("is-invalid")) {
                                selects[i].classList.add("is-invalid");
                            }
                        }
                    }
                }
            }

            if (!isError) {
                Swal.fire({
                    title: submitMessage(),
                    text: "",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#5bbc2e',
                    cancelButtonColor: '#d33',
                    confirmButtonText: yesMessage(),
                    cancelButtonText: noMessage()
                }).then((result) => {
                    if (result.value) {
                        formSubmitForm.submit();
                    }
                })
            } else {
                Swal.fire({
                    title: "<p><strong>" + mendatoryMessage() + "</strong></p>",
                    text: "",
                    icon: 'warning',
                    confirmButtonColor: '#5bbc2e',
                    confirmButtonText: okMessage()
                })

                //$('html, body').animate({
                //    scrollTop: $("#" + elementId).offset().top - 100
                //}, 1000); 
            }
        }
    }
});