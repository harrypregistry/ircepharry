﻿$(document).ready(function () {
    $.noConflict();
    $('#customTable').DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "ordering": true,
        "order": [[8, "desc"]],
        "lengthMenu": [25, 50, 100, 150],
        "pageLength": 25,
        "ajax": {
            "dataType": 'json',
            "type": "POST",
            "url": '/ApplicationUsers/IndexJson',
            "dataSrc": function (json) {
                $('#lblTotalCount').text(json.data.TotalCount);
                $('#lblTotalPregnants').text(json.data.TotalPregnants);
                $('#lblTotalNonPregnants').text(json.data.TotalNonPregnants);
                return json.data.Items;
            }
        },
        columns: [
            { data: "FirstName" },
            { data: "Email" },
            { data: "UserNumber" },
            { data: "RegisterCountry" },
            { data: "Country", bSortable: false },
            {
                data: "PendingForms",
                bSortable: false,
                render: function (data, type, row, meta) {
                    var data = "";
                    var i;
                    for (i = 0; i < row.PendingForms.length; i++) {
                        data += '<span class="badge badge-warning">' + row.PendingForms[i].CategoryName + '</span>';
                    }
                    return data;
                }
            },
            {
                data: "CompletedForms",
                bSortable: false,
                render: function (data, type, row, meta) {
                    var data = "";
                    var i;
                    for (i = 0; i < row.CompletedForms.length; i++) {
                        data += '<span class="badge badge-success">' + row.CompletedForms[i].CategoryName + '</span>';
                    }
                    return data;
                }
            },
            {
                data: "PhoneNumberConfirmed",
                bSortable: false,
                render: function (data, type, row, meta) {
                    if (row.PhoneNumberConfirmed) {
                        return '<span class="badge badge-success">Verified</span>';
                    } else {
                        return '<span class="badge badge-danger">Unverified</span>';
                    }
                }
            },
            {
                data: "ConsentSign",
                bSortable: false,
                render: function (data, type, row, meta) {
                    if (row.ConsentSign) {
                        return '<span class="badge badge-success">Verified</span>';
                    } else {
                        return '<span class="badge badge-danger">Unverified</span>';
                    }
                }
            },
            { data: "CreatedDateToString" },
            {
                'data': 'Action', "bSortable": false, 'render': function (data, type, row, meta) {
                    var UserId = row.Id;
                    var deleteAnchorLink = "/ApplicationUsers/Delete?id=" + UserId;
                    var incentiveLink = "/ApplicationUsers/Incentive?id=" + UserId;
                    var link = "<button type=\"button\" class=\"btn btn-sm btn-primary mt-1\" onclick=\"\">Send</button>";
                    link += "<a href=\"" + deleteAnchorLink + "\" class=\"btn btn-sm btn-danger mt-1\">Delete</a>";
                    console.log(row.Insentive);
                    if (row.Insentive === true) {
                        link += "<button type=\"button\" class=\"btn btn-sm btn-danger mt-1\" onclick=\"handleRemoveIncentive('" + UserId + "')\">Remove incentive</button>";
                    } else {
                        link += "<a href=\"" + incentiveLink + "\" class=\"btn btn-sm btn-primary mt-1\">Set incentive</a>";
                    }                   
                    return link;
                }
            }
        ]
    })
});




