﻿$(document).ready(function () {
    $.noConflict();
    $('#customTable').DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "ordering": true,
        "order": [[7, "desc"]],
        "lengthMenu": [25, 50, 100, 150],
        "pageLength": 25,
        "ajax": {
            "dataType": 'json',
            "type": "POST",
            "url": '/ApplicationUsers/VerifiedJson',
            "dataSrc": function (json) {
                return json.data.Items;
            }
        },
        columns: [
            { data: "FirstName" },
            { data: "UserNumber" },
            { data: "RegisterCountry", bSortable: false },
            { data: "Country" },
            {
                data: "TodoForms",
                bSortable: false,
                render: function (data, type, row, meta) {
                    var data = "";
                    var i;
                    for (i = 0; i < row.TodoForms.length; i++) {
                        data += '<span class="badge badge-primary">' + row.TodoForms[i].CategoryName + '</span>';
                    }
                    return data;
                }
            },
            {
                data: "PendingForms",
                bSortable: false,
                render: function (data, type, row, meta) {
                    var data = "";
                    var i;
                    for (i = 0; i < row.PendingForms.length; i++) {
                        data += '<span class="badge badge-warning">' + row.PendingForms[i].CategoryName + '</span>';
                    }
                    return data;
                }
            },
            {
                data: "CompletedForms",
                bSortable: false,
                render: function (data, type, row, meta) {
                    var data = "";
                    var i;
                    for (i = 0; i < row.CompletedForms.length; i++) {
                        data += '<span class="badge badge-success">' + row.CompletedForms[i].CategoryName + '</span>';
                    }
                    return data;
                }
            },
            {
                data: "CreatedDateToString"
            }
        ]
    })
});




