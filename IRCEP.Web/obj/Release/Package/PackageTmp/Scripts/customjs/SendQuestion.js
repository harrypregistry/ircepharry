﻿
$(document).ready(function () {
    var obj = {};
    obj.id = $('#hddUserId').val();

    $.ajax({
        type: "POST",
        url: "/Staff/UserAnswerList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(obj),
        async: false,
        success: function (response) {

            var childData = JSON.parse(response.data);

            var htmlleft = "";
            var htmlright = "";
            htmlMsg = "";

            var i;
            var j;

            for (i = 0; i < childData.length; i++) {

                var uRole = childData[i].UserRole;
                if (uRole != 'Pregnant') {
                    var datetime = moment(new Date(childData[i].CreatedDate)).format("MMM") + ' ' + moment(new Date(childData[i].CreatedDate)).format("DD") + ' ' + moment(new Date(childData[i].CreatedDate)).format("hh:mm A");

                    htmlleft += '<div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">' + uRole + '</span><span style="padding-left: 10px" class="direct-chat-timestamp float-right">' + datetime + '</span> </div><div class="direct-chat-text"> ' + childData[i].Question + '</div>';



                    for (j = 0; j < childData[i].Answers.length; j++) {

                        var answerlst = childData[i].Answers;

                        var datetime = moment(new Date(answerlst[j].CreatedDate)).format("MMM") + ' ' + moment(new Date(answerlst[j].CreatedDate)).format("DD") + ' ' + moment(new Date(answerlst[j].CreatedDate)).format("hh:mm A");


                        htmlleft += '<div  class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">' + answerlst[j].UserNumber + '</span><span style="padding-left: 10px" class="direct-chat-timestamp float-left">' + datetime + '</span></div><div class="direct-chat-text">' + answerlst[j].Answer + '</div> </div>';
                        //
                        //$('#hdduserName').val(answerlst[j].FullName);
                        $('#hdduserName').val(uRole);
                    }

                    $('#hddQuestionId').val(childData[i].Id);
                }
            }
            htmlMsg = '<div class="card direct-chat direct-chat-primary"><div class="card-body"> <div id="divmsg" style="height:400px" class="direct-chat-messages">';
            htmlMsg += '<div class="direct-chat-msg">' + htmlleft;

            htmlMsg += '</div><div id="divrightSide"></div> ';

            htmlMsg += '</div></div></div></div> <div class="card-footer"><div class="input-group"><input type="text" id="txtMsg" name="txtMsg" placeholder="Type Message ..." class="form-control"><span class="input-group-append"><button type="button"  onclick="QuestionMessage();" class="btn btn-primary">Send</button></span></div></div>';



            $('#divChat').append(htmlMsg);

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });

    $("#divmsg").animate({
        scrollTop: $('#divmsg')[0].scrollHeight - $('#divmsg')[0].clientHeight
    }, 1000);


});

function QuestionMessage() {
    var d = new Date();
    var obj = {};
    obj.UserId = $('#hddUserId').val();
    obj.mesaage = $('#txtMsg').val();



    if (obj.mesaage.trim() == '') {
        alert('Please write message!');
    } else {

        var datetime = moment(new Date(d)).format("MMM") + ' ' + moment(new Date(d)).format("DD") + ' ' + moment(new Date(d)).format("hh:mm A");

        $.ajax({
            type: "POST",
            url: "/Staff/UserQuestion",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(obj),
            async: false,
            success: function (response) {


                var htmlright = '<div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">' + $('#hdduserName').val() + '</span><span style="padding-left: 10px" class="direct-chat-timestamp float-left">' + datetime + '</span></div><div class="direct-chat-text">' + $('#txtMsg').val() + '</div>';

                $('#divrightSide').append(htmlright);

                $('#txtMsg').val('')

                //alert(response.data);
            },
            failure: function (response) {
                //alert(response.d);
            },
            error: function (response) {
                //alert(response.d);
            }
        });

    }
}
