﻿function postUnCheckedRequest(viewModel) {
    $.ajax({
        type: "POST", dataType: 'json', url: "/Survey/PostUnChecked",
        cache: false,
        data: {
            CategoryId: viewModel.CategoryId,
            QuestionId: viewModel.QuestionId,
            QuestionOptionId: viewModel.QuestionOptionId,
            FormInputId: viewModel.FormInputId,
            Text: viewModel.Text,
            Name: viewModel.Name,
            QuestionSubmitId: viewModel.QuestionSubmitId,
            FormSubmitId: viewModel.FormSubmitId,
            Language: document.getElementsByTagName("html")[0].getAttribute("lang"),
            __RequestVerificationToken: gettoken()
        },
        success: handleUnCheckCallback
    });
}

function PostRequest(viewModel) {
    $.ajax({
        type: "POST", dataType: 'json', url: "/Survey/PostRequest",
        cache: false,
        data: {
            CategoryId: viewModel.CategoryId,
            QuestionId: viewModel.QuestionId,
            QuestionOptionId: viewModel.QuestionOptionId,
            FormInputId: viewModel.FormInputId,
            Text: viewModel.Text,
            Name: viewModel.Name,
            QuestionSubmitId: viewModel.QuestionSubmitId,
            FormSubmitId: viewModel.FormSubmitId,
            Language: document.getElementsByTagName("html")[0].getAttribute("lang"),
            __RequestVerificationToken: gettoken()
        },
        success: handleCallBack
    });
}

function PostBabyRequest(viewModel) {
    $.ajax({
        type: "POST", dataType: 'json', url: "/Baby/PostRequest",
        cache: false,
        data: {
            CategoryId: viewModel.CategoryId,
            QuestionId: viewModel.QuestionId,
            QuestionOptionId: viewModel.QuestionOptionId,
            FormInputId: viewModel.FormInputId,
            Text: viewModel.Text,
            Name: viewModel.Name,
            QuestionSubmitId: viewModel.QuestionSubmitId,
            FormSubmitId: viewModel.FormSubmitId,
            Language: document.getElementsByTagName("html")[0].getAttribute("lang"),
            __RequestVerificationToken: gettoken()
        },
        success: handleCallBack
    });
}

function postBabyUnCheckedRequest(viewModel) {
    $.ajax({
        type: "POST", dataType: 'json', url: "/Baby/PostUnChecked",
        cache: false,
        data: {
            CategoryId: viewModel.CategoryId,
            QuestionId: viewModel.QuestionId,
            QuestionOptionId: viewModel.QuestionOptionId,
            FormInputId: viewModel.FormInputId,
            Text: viewModel.Text,
            Name: viewModel.Name,
            FormSubmitId: viewModel.FormSubmitId,
            QuestionSubmitId: viewModel.QuestionSubmitId,
            Language: document.getElementsByTagName("html")[0].getAttribute("lang"),
            __RequestVerificationToken: gettoken()
        },
        success: handleUnCheckCallback
    });
}


function handleCallBack(resp) {
    if (!resp.Success) {
        return false;
    }

    var addToParent = false;
    var parentFormDiv = document.getElementById(resp.ParentFormDiv);
    var nestedFormDivEle = document.getElementById(resp.NestedFormDiv); 
    if (nestedFormDivEle !== null) {
        if (nestedFormDivEle.classList.contains("animated")) {
            nestedFormDivEle.classList.remove("animated");
        }
        if (nestedFormDivEle.classList.contains("fadeInDown")) {
            nestedFormDivEle.classList.remove("fadeInDown");
        }
    }
    if (nestedFormDivEle !== null) { if (nestedFormDivEle.children.length > 0) { while (nestedFormDivEle.lastChild) { nestedFormDivEle.removeChild(nestedFormDivEle.lastChild); } } }
    if (!resp.IsChildren) {
        UpdateCategoryProgress();
        return false;
    }
    if (nestedFormDivEle === null) {
        nestedFormDivEle = document.createElement('div');
        nestedFormDivEle.setAttribute('id', resp.NestedFormDiv);
        nestedFormDivEle.className = "animated fadeInDown";
        nestedFormDivEle.innerHTML = resp.HtmlView;
        addToParent = true;
    } else {
        nestedFormDivEle.className = "animated fadeInDown";
        nestedFormDivEle.innerHTML = resp.HtmlView;
    }

    if (addToParent) {
        parentFormDiv.appendChild(nestedFormDivEle);
    }    
    UpdateCategoryProgress();
}


function handleUnCheckCallback(resp) {
    var nodeElement = document.getElementById(resp.ParentFormDiv);
    if (nodeElement !== null) {
        if (nodeElement.classList.contains("animated")) {
            nodeElement.classList.remove("animated");
        }
        if (nodeElement.classList.contains("fadeInDown")) {
            nodeElement.classList.remove("fadeInDown");
        }
    }
    if (nodeElement !== null) {
        if (nodeElement.children.length > 0) {
            while (nodeElement.lastChild) {
                nodeElement.removeChild(nodeElement.lastChild);
            }
        }
    }
    UpdateCategoryProgress();
}

function handleClick(_this) {

    const type = _this.getAttribute("type");
    const dataType = _this.getAttribute('data-type');
    const dataName = _this.getAttribute('data-name');
    const questionId = _this.getAttribute('data-questionId');
    const Id = _this.getAttribute('Id');
    const required = _this.getAttribute("data-required");
    const divName = _this.getAttribute("data-divName");

    const inputValue = _this.value;
    var viewModel = {
        CategoryId: _this.getAttribute("data-categoryId"),
        QuestionId: _this.getAttribute("data-questionId"),
        QuestionOptionId: _this.getAttribute("data-optionId"),
        FormInputId: _this.getAttribute("data-formInputId"),
        QuestionSubmitId: _this.getAttribute("data-questionSubmitId"),
        FormSubmitId: _this.getAttribute("data-formSubmitId"),
        Text: _this.value,
        Name: dataName !== null ? dataName : ""
    };


    if (questionId === "502") {
        if (type === "text") {
            var a = document.querySelectorAll('[data-label-name]');

            for (var i in a) if (a.hasOwnProperty(i)) {
                if (dataName === a[i].getAttribute('data-label-name')) {
                    
                    var notReplaceLabelId = "214-" + a[i].getAttribute('data-label-name') + "-" + "label"; 
                    var replacedLabelId = a[i].getAttribute('id');
                    if (notReplaceLabelId !== replacedLabelId) {
                        var raplaceBabyName = "";
                        var replaceText = "";
                        var dataQuestionId = a[i].getAttribute('data-questionId');
                        var babyName = a[i].getAttribute('data-baby-name');
                        var labeltext = (a[i].innerText || a[i].textContent);
                        var questions = [];
                        if (inputValue === "") {
                            var data = { categoryId: 11, questionId: parseInt(dataQuestionId) };
                            $.ajax({
                                dataType: "json",
                                url: "/Survey/GetQuestions",
                                data: data,
                                async: false,
                                success: function (data) {
                                    questions.push(data);
                                }
                            });
                            if (questions.length > 0) {
                                a[i].textContent = questions[0].QuestionText;
                                a[i].setAttribute('data-baby-name', '');
                            }
                        } else {
                            raplaceBabyName = babyName === "" ? "your baby" : babyName;
                            var isYourBabyInclude = labeltext.includes(raplaceBabyName);

                            raplaceBabyName = babyName === "" ? "the baby" : babyName;
                            var isTheBabyInclude = labeltext.includes(raplaceBabyName);

                            raplaceBabyName = babyName === "" ? "baby" : babyName;
                            var isBabyInclude = labeltext.includes(raplaceBabyName);

                            if (isYourBabyInclude) {
                                replaceText = labeltext.replace(raplaceBabyName, inputValue);
                                a[i].setAttribute('data-baby-name', inputValue);
                                a[i].textContent = replaceText;
                            } else if (isTheBabyInclude) {
                                replaceText = labeltext.replace("the baby", inputValue);
                                a[i].setAttribute('data-baby-name', inputValue);
                                a[i].textContent = replaceText;
                            } else if (isBabyInclude) {
                                replaceText = labeltext.replace("baby", inputValue);
                                a[i].setAttribute('data-baby-name', inputValue);
                                a[i].textContent = replaceText;
                            }
                        }   
                    }                    
                }
            }
        }        
    }

    if (questionId === "1691") {
        if (type === "text") {
            var a = document.querySelectorAll('[data-label-name]');

            for (var i in a) if (a.hasOwnProperty(i)) {
                if (dataName === a[i].getAttribute('data-label-name')) {

                        var raplaceBabyName = "";
                        var replaceText = "";
                        var dataQuestionId = a[i].getAttribute('data-questionId');
                        var babyName = a[i].getAttribute('data-baby-name');
                        var labeltext = (a[i].innerText || a[i].textContent);
                        var questions = [];
                        if (inputValue === "") {
                            var data = { categoryId: 38, questionId: parseInt(dataQuestionId) };
                            $.ajax({
                                dataType: "json",
                                url: "/Survey/GetQuestions",
                                data: data,
                                async: false,
                                success: function (data) {
                                    questions.push(data);
                                }
                            });
                            if (questions.length > 0) {
                                a[i].textContent = questions[0].QuestionText;
                                a[i].setAttribute('data-baby-name', '');
                            }
                        } else {
                            raplaceBabyName = babyName === "" ? "your baby" : babyName;
                            var isYourBabyInclude = labeltext.includes(raplaceBabyName);

                            raplaceBabyName = babyName === "" ? "the baby" : babyName;
                            var isTheBabyInclude = labeltext.includes(raplaceBabyName);

                            raplaceBabyName = babyName === "" ? "baby" : babyName;
                            var isBabyInclude = labeltext.includes(raplaceBabyName);

                            if (isYourBabyInclude) {
                                replaceText = labeltext.replace(raplaceBabyName, inputValue);
                                a[i].setAttribute('data-baby-name', inputValue);
                                a[i].textContent = replaceText;
                            } else if (isTheBabyInclude) {
                                replaceText = labeltext.replace("the baby", inputValue);
                                a[i].setAttribute('data-baby-name', inputValue);
                                a[i].textContent = replaceText;
                            } else if (isBabyInclude) {
                                replaceText = labeltext.replace("baby", inputValue);
                                a[i].setAttribute('data-baby-name', inputValue);
                                a[i].textContent = replaceText;
                            }
                        }
              
                }
            }
        }
    }

    if (type === "text") {
        
        if (dataType === "numeric") {
            if (questionId === "138") {
                if (parseInt(_this.value) < 20 && parseInt(_this.value) > 45) {
                    Swal.fire({
                        title: "<p><strong>" + getRange20To45() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }

            } else if (questionId === "689" || questionId === "2218") {
                if (parseInt(_this.value) < 500 || parseInt(_this.value) > 6000) {
                    Swal.fire({
                        title: "<p><strong>" + getRange500To6000() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }

            } else if (questionId === "688" || questionId === "2217") {
                if (parseFloat(_this.value) < 1 || parseFloat(_this.value) > 11) {
                    Swal.fire({
                        title: "<p><strong>" + getRange1To11() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }
            } else if (questionId === "1019" || questionId === "1020" || questionId === "1057" || questionId === "1058" || questionId === "1912" || questionId === "1908") {
                if (parseFloat(_this.value) < 4 || parseFloat(_this.value) > 16) {
                    Swal.fire({
                        title: "<p><strong>" + getRange4To16() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }
            } else if (questionId === "1023" || questionId === "1024" || questionId === "1061" || questionId === "1062" || questionId === "1913" || questionId === "1909") {
                if (parseInt(_this.value) < 2000 || parseInt(_this.value) > 8000) {
                    Swal.fire({
                        title: "<p><strong>" + getRange2000To8000() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }

            } else if (questionId === "2272" || questionId === "2276") {
                if (parseInt(_this.value) < 500 || parseInt(_this.value) > 10000) {
                    Swal.fire({
                        title: "<p><strong>" + getRange500To10000() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }
            } else if (questionId === "2271" || questionId === "2275") {
                if (parseInt(_this.value) < 1 || parseInt(_this.value) > 22) {
                    Swal.fire({
                        title: "<p><strong>" + getRange1To22() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }
            } else if (questionId === "2476" || questionId === "2477" || questionId === "2478" || questionId === "2479" || questionId === "2480" || questionId === "2481" || questionId === "2482" || questionId === "2483") {
                if (parseInt(_this.value) < 0 || parseInt(_this.value) > 15) {
                    Swal.fire({
                        title: "<p><strong>" + getRange0To13() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }
            }
        } else if (dataType === "datepicker") {
            //if (!_this.value.length > 6) {
            //    if (questionId === 136) {
            //        var varDate = inputValue.split('/');
            //        var inputDate = moment(varDate[2] + "-" + varDate[0] + "-" + varDate[1], "YYYY-MM-DD").format("YYYY-MM-DD");
            //        var todayDate = moment().format("YYYY-MM-DD");
            //        if (inputDate > todayDate) {
            //            _this.value = '';
            //            return false;
            //        }
            //    }
            //}
        } else if (dataType === "textarea") {
            //if (_this.value.length > 250) {                
            //    _this.value = _this.value.slice(0, 250);
            //    _this.className = "form-control is-invalid";
            //    alert("Please enter character less than 250");
            //    return false;
            //} else {
            //    _this.className = "form-control is-valid";
            //}
        }

        if (required === "true") {            
            if (_this.classList.length > 0) {
                if (_this.classList.contains("is-invalid")) {
                    _this.classList.remove("is-invalid");
                    _this.classList.add("is-valid");
                }
            }
        }
    } else if (dataType === "select") {
        if (required === "true") {
            if (_this.classList.length > 0) {
                if (_this.classList.contains("is-invalid")) {
                    _this.classList.remove("is-invalid");
                    _this.classList.add("is-valid");
                }
            }            
        }

        viewModel.QuestionOptionId = _this.value;
        viewModel.Text = _this.options[_this.selectedIndex].text;
    }

    if (type === "checkbox" || type === "radio") {
        var parentEle = document.getElementById(divName + questionId + "-div");
        if (parentEle.classList.length > 0) {
            if (parentEle.classList.contains("custom-val-color-r")) {
                parentEle.classList.remove("custom-val-color-r");
                parentEle.classList.add("custom-val-color-g");
            }
        } else {
            parentEle.classList.remove("custom-val-color-r");
        }         
    }

    if (type === "checkbox") {
        if (_this.checked) { PostRequest(viewModel); }
        else { postUnCheckedRequest(viewModel); }
    } else {
        PostRequest(viewModel);
        if (viewModel.CategoryId === "8" || viewModel.CategoryId === "37") {
            if (viewModel.CategoryId === "37") {

                if (($('#11800').prop('checked') === true && $('#19652').prop('checked') === true) || ($('#11800').prop('checked') === true && $('#19654').prop('checked') === true)) {
                    isRequired = false;
                    Swal.fire({
                        title: weAreSorryText(),
                        text: covidEligibleText(),
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#5BBC2E',
                        cancelButtonColor: '#d33',
                        confirmButtonText: leaveTheStudyText(),
                        cancelButtonText: editAnswersText()
                    }).then((result) => {
                        if (result.value) {
                            document.getElementById("formSubmit").submit();
                        }
                    })
                }
            } else if (viewModel.CategoryId === "8") {
                if (($('#260').prop('checked') === true && $('#19651').prop('checked') === true) || ($('#260').prop('checked') === true && $('#19653').prop('checked') === true)) {
                    isRequired = false;
                    Swal.fire({
                        title: weAreSorryText(),
                        text: covidEligibleText(),
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#5BBC2E',
                        cancelButtonColor: '#d33',
                        confirmButtonText: leaveTheStudyText(),
                        cancelButtonText: editAnswersText()
                    }).then((result) => {
                        if (result.value) {
                            document.getElementById("formSubmit").submit();
                        }
                    })
                }
            }
        }
    }
}

function handleBabyClick(_this) {

    const type = _this.getAttribute("type");
    const dataType = _this.getAttribute('data-type');
    const dataName = _this.getAttribute('data-name');
    const questionId = _this.getAttribute("data-questionId");
    const divName = _this.getAttribute("data-divName");
    const required = _this.getAttribute("data-required");

    var viewModel = {
        CategoryId: _this.getAttribute("data-categoryId"),
        QuestionId: _this.getAttribute("data-questionId"),
        QuestionOptionId: _this.getAttribute("data-optionId"),
        FormInputId: _this.getAttribute("data-formInputId"),
        QuestionSubmitId: _this.getAttribute("data-questionSubmitId"),
        Text: _this.value,
        FormSubmitId: _this.getAttribute('data-formSubmitId'),
        Name: dataName !== null ? dataName : ""
    };

    
    if (type === "text") {
        if (dataType === "numeric") {
            if (questionId === "1019" || questionId === "1020" || questionId === "1057" || questionId === "1058" || questionId === "1912" || questionId === "1908" || questionId === "2271" || questionId === "2275") {
                if (parseFloat(_this.value) < 4 || parseFloat(_this.value) > 16) {
                    Swal.fire({
                        title: "<p><strong>" + getRange4To16() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }
            } else if (questionId === "1023" || questionId === "1024" || questionId === "1061" || questionId === "1062" || questionId === "1913" || questionId === "1909") {
                if (parseInt(_this.value) < 2000 || parseInt(_this.value) > 8000) {
                    Swal.fire({
                        title: "<p><strong>" + getRange2000To8000() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }

            } else if (questionId === "2272" || questionId === "2276") {
                if (parseInt(_this.value) < 2000 || parseInt(_this.value) > 7000) {
                    Swal.fire({
                        title: "<p><strong>" + getRange2000To7000() + "</strong></p>",
                        text: "",
                        icon: 'warning',
                        confirmButtonColor: '#5bbc2e',
                    })
                    _this.value = '';
                    return false;
                }
            }
        } else if (dataType === "textarea") {
            //if (_this.value.length > 250) {
            //    _this.value = _this.value.slice(0, 250);
            //    _this.className = "form-control is-invalid";
            //    return false;
            //} else {
            //    _this.className = "form-control is-valid";
            //}
        }

        if (required === "true") {
            if (_this.classList.length > 0) {
                if (_this.classList.contains("is-invalid")) {
                    _this.classList.remove("is-invalid");
                    _this.classList.add("is-valid");
                }
            }
        }

    } else if (dataType === "select") {
        viewModel.QuestionOptionId = _this.value;
        viewModel.Text = _this.options[_this.selectedIndex].text;

        if (required === "true") {
            if (_this.classList.length > 0) {
                if (_this.classList.contains("is-invalid")) {
                    _this.classList.remove("is-invalid");
                    _this.classList.add("is-valid");
                }
            }
        }

    }

    if (type === "checkbox" || type === "radio") {
        var parentEle = document.getElementById(divName + questionId + "-div");
        if (parentEle.classList.length > 0) {
            if (parentEle.classList.contains("custom-val-color-r")) {
                parentEle.classList.remove("custom-val-color-r");
                parentEle.classList.add("custom-val-color-g");
            }
        } else {
            parentEle.classList.remove("custom-val-color-r");
        }
    }

    if (type === "checkbox") {
        if (_this.checked) { PostBabyRequest(viewModel); }
        else { postBabyUnCheckedRequest(viewModel); }
    } else { PostBabyRequest(viewModel); }
}

BindLoader();

function UploadFiles(_this) {
    var files = $(_this).get(0).files;
    var fileData = new FormData();

    for (var i = 0; i < files.length; i++) { fileData.append(files[i].name, files[i]); }

    var viewModel = {
        CategoryId: _this.getAttribute("data-categoryId"),
        QuestionId: _this.getAttribute("data-questionId"),
        QuestionOptionId: _this.getAttribute("data-optionId"),
        FormInputId: _this.getAttribute("data-formInputId"),
        FormSubmitId: $("#" + id).attr("data-formSubmitId")
    };
    fileData.append('obj', JSON.stringify(viewModel));

    $.ajax({
        type: "POST",
        url: "/Survey/UploadFiles",
        dataType: "json",
        contentType: false, // Not to set any content header
        processData: false, // Not to process data
        data: fileData,
        success: function (result, status, xhr) {
            alert(result);
        },
        error: function (xhr, status, error) {
            alert(status);
        }
    });
}

function ShowUploader(id) {
    $('#' + id).trigger('click');
}

function UploadFiles(_this) {  
    SendFiles($(_this).get(0).files, _this.getAttribute("id"));   
}

function BindLoader() {
    $('.fileuploader-input').each(function (i, obj) {
        var id = $('.fileuploader-input').eq(i).attr('id');
        const dropArea = document.getElementById(id);
        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach((eventName) => {
            dropArea.addEventListener(eventName, preventDefaults, false);
        });

        ['dragenter', 'dragover'].forEach((eventName) => {
            dropArea.addEventListener(eventName, highlight, false);
        });

        ['dragleave', 'drop'].forEach((eventName) => {
            dropArea.addEventListener(eventName, unHightLight, false);
        });

        dropArea.addEventListener('drop', handleDrop, false);

        function preventDefaults(e) {
            e.preventDefault()
            e.stopPropagation()
        }

        function highlight(e) {
            dropArea.classList.add('highlight')
        }

        function unHightLight(e) {
            dropArea.classList.remove('highlight')
        }

        function handleDrop(e) {
  
            var dt = e.dataTransfer
            var files = dt.files

            SendFiles(files, id.replace('-fileuploader', ''))
        }
    });
}

function SendFiles(files, id) {
    if (files.length > 0) {
        document.getElementById("loading-picture").style.display = "block";

        var fileData = new FormData();
        var filesNameList = "";
        var fSExt = new Array('Bytes', 'KB', 'MB');
        
        for (var i = 0; i < files.length; i++) {

            var ext = files[i].name.replace(/^.*\./, '').toLowerCase();

            if (ext === "jpg" || ext === "jpeg" || ext === "png" || ext === "pdf") {

                var sizemsg = "";
                var _size = files[i].size;
                if (_size > 4000000) {
                    sizeLimit = true;
                    sizemsg = "<span style='color: red;'>Try to upload file less than 4MB!</span>";

                    j = 0; while (_size > 900) { _size /= 1024; j++; }
                    var exactSize = (Math.round(_size * 100) / 100) + ' ' + fSExt[j];

                    var css = 'file-type-image file-ext-' + ext;
                    if (ext === "pdf") css = 'file-type-application file-ext-' + ext;
                    filesNameList +=
                        '<li class="fileuploader-item ' + css + '">' +
                        '<div class="columns">' +
                        '<div class="column-thumbnail">' +
                        '<div class="fileuploader-item-image">' +
                        '<div class="fileuploader-item-icon">' +
                        '<i>' + ext + ' </i>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="column-title">' +
                        '<div title="' + files[i].name + '">' + files[i].name + '</div>' +
                        '<span>' + exactSize + '</span> ' + sizemsg +
                        '</div>' +
                        '<div class="column-actions">' +
                        '<button type="button" onclick="removeFileFailed(this)" class="fileuploader-action fileuploader-action-remove" title="Delete"><i class="fileuploader-icon-remove"></i></button>' +
                        '</div>' +
                        '</div>' +
                        '</li>';
                }
                else {
                    fileData.append(files[i].name, files[i]);
                }
            }
        }

        if (filesNameList.length > 0) $("#" + id + "-files").prepend(filesNameList);


        var viewModel = {
            CategoryId: $("#" + id).attr("data-categoryId"),
            QuestionId: $("#" + id).attr("data-questionId"),
            QuestionOptionId: $("#" + id).attr("data-optionId"),
            FormInputId: $("#" + id).attr("data-formInputId"),
            Name: $("#" + id).attr("data-name"),
            FormSubmitId: $("#" + id).attr("data-formSubmitId")
        };
        fileData.append('obj', JSON.stringify(viewModel));
        $.ajax({
            type: "POST",
            url: "/Survey/UploadFiles",
            dataType: "json",
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: fileData,
            success: function (data) {
                document.getElementById("loading-picture").style.display = "none";
                if (data.status === "success") { $("#" + id + "-files").prepend(data.message) }
                else alert(data.message);                
            },
            error: function (xhr, status, error) {
                alert(status);
                document.getElementById("loading-picture").style.display = "none";
                console.log(xhr.responseText);
            }
        });
    }
}

function removeFile(_this, id) {

    $.ajax({
        type: "POST", dataType: 'json', url: "/Survey/DeleteFiles",
        data: { id: id },
        success: function (data) {
            if (data.status === "success") {
                $(_this).closest('.fileuploader-item').remove();
            }
        },
        error: function (xhr, status, error) {
            alert(status);
            console.log(xhr.responseText);
        }
    });
}

function removeFileFailed(_this) {
    $(_this).closest('.fileuploader-item').remove();
}

function UpdateCategoryProgress() {
    var category_Id = document.getElementById("CategoryId").value;

    if (parseInt(category_Id) === 13) {
        console.log("false");
        return false;
    }
    var prog = 0;
    var $list = $("label.required");
    var totalProg = $list.length;
    
    const inputs = document.getElementsByTagName("input");
    const selects = document.getElementsByTagName("select");
    var uniqueName = "";

    for (var i in inputs) if (inputs.hasOwnProperty(i)) {
        const required = inputs[i].getAttribute("data-required");
        if (required !== null || required !== "") {
            if (required === "true") {
                const type = inputs[i].getAttribute("type");
                if (type === "text") {
                    const value = inputs[i].value;
                    if (value !== "" || value !== null) {
                        prog++;
                    }
                
                }
                
                if (type === "radio" || type === "checkbox") {
                    const name = inputs[i].getAttribute('name');
                    if (uniqueName != name) {
                        var groupName = document.getElementsByName(name);
                        var anyChecked = false;
                        for (i = 0; i < groupName.length; i++) {
                            if (groupName[i].checked) {
                                anyChecked = true;
                            }
                        }
                        if (anyChecked) {
                            prog++;
                        }
                        uniqueName = name;
                    }                    
                }
            }
        }
    }

    for (var i in selects) if (selects.hasOwnProperty(i)) {
        const required = selects[i].getAttribute("data-required");
        if (required !== null || required !== "") {
            if (required === "true") {
                var selectValue = selects[i].value;
                if (selectValue !== "") {
                    prog++;
                }
            }
        }
    }

    //var dProgress = (prog / totalProg) * 100.0;
    //var bar1 = new ldBar("#ProgressBar");
    //bar1.set(dProgress);


    $.ajax({
        type: "POST", dataType: 'json', url: "/Survey/SaveProgress",
        cache: false,
        data: {
            CategoryId: document.getElementById("CategoryId").value,
            FormSubmitId: document.getElementById("FormSubmitId").value,
            Progress: prog,
            TotalRequired: totalProg
        }
    });
}


