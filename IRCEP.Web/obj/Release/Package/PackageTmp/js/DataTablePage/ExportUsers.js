﻿$(document).ready(function () {
    $.noConflict();
    $('#customTable').DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "ordering": true,
        "order": [[6, "desc"]],
        "lengthMenu": [25, 50, 100, 150],
        "pageLength": 25,
        "ajax": {
            "dataType": 'json',
            "type": "POST",
            "url": '/DataExport/GetJson',
            "dataSrc": function (json) {
                return json.data.Items;
            }
        },
        columns: [
            { data: "UserNumber" },
            { data: "RegisterCountry" },
            { data: "Country" },
            {
                data: "TodoForms",
                render: function (data, type, row, meta) {
                    var data = "";
                    var i;
                    for (i = 0; i < row.TodoForms.length; i++) {
                        data += '<span class="badge badge-primary">' + row.TodoForms[i].CategoryName + '</span>';
                    }
                    return data;
                }
            },
            {
                data: "PendingForms",
                bSortable: false,
                render: function (data, type, row, meta) {
                    var data = "";
                    var i;
                    for (i = 0; i < row.PendingForms.length; i++) {
                        data += '<span class="badge badge-warning">' + row.PendingForms[i].CategoryName + '</span>';
                    }
                    return data;
                }
            },
            {
                data: "CompletedForms",
                bSortable: false,
                render: function (data, type, row, meta) {
                    var spanList = "";
                    var i;
                    for (i = 0; i < row.CompletedForms.length; i++) {
                        var anchorLink = "/DataExport/Print?formSubmitId=" + row.CompletedForms[i].Id + "&categoryId=" + row.CompletedForms[i].CategoryId + "&userNumber=" + row.UserNumber;
                        var span = "<a href=\"" + anchorLink + "\" class=\"badge badge-success\" target=\"_blank\">" + row.CompletedForms[i].CategoryName + "</a>"
                        spanList += span;
                    }
                    return spanList;
                }

            },
            { data: "CreatedDateToString" }
        ]
    })
});


