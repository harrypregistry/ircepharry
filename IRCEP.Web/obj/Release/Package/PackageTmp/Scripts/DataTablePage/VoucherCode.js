﻿$(document).ready(function () {
    $.noConflict();

    $('#customTable').DataTable({
        "ajax": {
            "dataType": 'json',
            "type": "GET",
            "url": '/VoucherCodes/IndexJson',
            "dataSrc": function (json) {
                return json.Items;
            }
        },
        columns: [
            { data: "Code" },
            {
                data: "Price",
                render: function (data, type, row, meta) {
                    return row.Currency + row.Price;
                }
            },
            { data: "Country" },
            {
                data: "IsAccess",
                render: function (data, type, row, meta) {
                    if (row.IsAccess !== null && row.IsAccess === true) {
                        return '<span class="badge badge-success">Radeemed</span>';
                    }
                    return '';
                }
            },
            {
                'data': 'Action', "bSortable": false, 'render': function (data, type, row, meta) {
                    var EditAnchorLink = "/VoucherCodes/Edit?id=" + row.Id;
                    var deleteAnchorLink = "/VoucherCodes/Delete?id=" + row.Id;
                    var link = "<a href=\"" + EditAnchorLink + "\" class=\"btn btn-sm btn-danger\">Edit</a>";
                    link += "<a href=\"" + deleteAnchorLink + "\" class=\"btn btn-sm btn-danger\">Delete</a>";
                    return link;
                }
            }
        ]
    })
});




