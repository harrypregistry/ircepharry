﻿
(function ($) {
    $("#LanguageS").on('click', 'ul li', function (evt) {
        evt.preventDefault();

        var t = window.location.pathname.replace(new RegExp('/' + $('#current li:first-child').attr('data-locale'), 'g'), "");
        var queryString = window.location.search;
        var dataLocale = $(this).attr('data-locale');
        if (queryString !== "") {
            window.location.href = "/" + dataLocale + t + queryString;
        } else {
            window.location.href = "/" + dataLocale + t;
        }
    });

    $("#widget").hover(function () { $(this).addClass('active') }, function () { $(this).removeClass('active') });

    var i = $(".classy-nav-container"),
        o = $(".classynav ul"),
        n = $(".classynav > ul > li"),
        s = $(".classy-navbar-toggler"),
        r = $(".classycloseIcon"),
        a = $(".navbarToggler"),
        l = $(".classy-menu"),
        c = $(window);

    s.on("click", function () { a.toggleClass("active"), l.toggleClass("menu-on") });
    r.on("click", function () { l.removeClass("menu-on"), a.removeClass("active") });
    n.has(".dropdown").addClass("cn-dropdown-item"), n.has(".megamenu").addClass("megamenu-item"), o.find("li a").each(function () { $(this).next().length > 0 && ($(this).parent("li").addClass("has-down"), $(this).parent("li").addClass("has-down")) });
    c.on("resize", function () { l.removeClass("menu-on"), a.removeClass("active") });
})(jQuery);