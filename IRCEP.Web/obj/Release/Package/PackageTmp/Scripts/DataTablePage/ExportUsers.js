﻿$(document).ready(function () {
    $.noConflict();

    

    $('#customTable').DataTable({
        "ajax": {
            "dataType": 'json',
            "type": "GET",
            "url": '/ApplicationUsers/GetAllUser',
            "dataSrc": function (json) {

               return json.Items;
            }
        },
        
        columns: [
            { data: "UserNumber" },           
            { data: "RegisterCountry" },
            { data: "Country" },
            {
                data: "TodoForms",
                render: function (data, type, row, meta) {
                    var pendiggData = "";
                    var i;
                    for (i = 0; i < row.TodoForms.length; i++) {                        
                        pendiggData += '<span class="badge badge-primary">' + row.TodoForms[i].CategoryName + '</span>';

                    }
                    return pendiggData;
                }

            },
            {
                data: "PendingForms",
                render: function (data, type, row, meta) {
                    var pendiggData = "";
                    var i;
                    for (i = 0; i < row.PendingForms.length; i++) {

                        pendiggData += '<span class="badge badge-warning">' + row.PendingForms[i].CategoryName + '</span>';


                    }

                    return pendiggData;
                }

            },
            {
                data: "CompletedForms",
                render: function (data, type, row, meta) {

                    var spanList = "";
                    var i;
                    for (i = 0; i < row.CompletedForms.length; i++) {

                        debugger
                        var anchorLink = "/DataExport/Print?formSubmitId=" + row.CompletedForms[i].Id + "&categoryId=" + row.CompletedForms[i].CategoryId + "&userNumber=" + row.UserNumber;
                        var span = "<a href=\"" + anchorLink + "\" class=\"badge badge-success\" target=\"_blank\">" + row.CompletedForms[i].CategoryName + "</a>"
                        spanList += span;

                    }

                   
                    
                    return spanList;
                }

            },

                    
            { data: "CreatedDateToString" }
            
        ]
    })




});




