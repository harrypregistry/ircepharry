﻿$(document).ready(function () {
    $.noConflict();



    $('#customTable').DataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "destroy": true,
        "ordering": true,
        "ajax": {
            "dataType": 'json',
            "type": "post",
            "url": '/Staff/GetAllUser',
            "dataSrc": function (json) {

                return json.data.Items;
            }
        },

        columns: [
            { data: "UserNumber" },
            { data: "RegisterCountry" },
            { data: "Country" },
            {
                data: "TodoForms",
                render: function (data, type, row, meta) {
                    var pendiggData = "";
                    var i;
                    for (i = 0; i < row.TodoForms.length; i++) {
                        pendiggData += '<span class="badge badge-primary">' + row.TodoForms[i].CategoryName + '</span>';

                    }
                    return pendiggData;
                }

            },
            {
                data: "PendingForms",
                render: function (data, type, row, meta) {
                    var pendiggData = "";
                    var i;
                    for (i = 0; i < row.PendingForms.length; i++) {

                        pendiggData += '<span class="badge badge-warning">' + row.PendingForms[i].CategoryName + '</span>';


                    }

                    return pendiggData;
                }

            },
            {
                data: "CompletedForms",
                render: function (data, type, row, meta) {

                    var spanList = "";
                    var i;
                    for (i = 0; i < row.CompletedForms.length; i++) {

                        
                        
                        var anchorLink = "/DataExport/Print?formSubmitId=" + row.CompletedForms[i].Id + "&categoryId=" + row.CompletedForms[i].CategoryId + "&userNumber=" + row.UserNumber;
                        var span = "<a href=\"" + anchorLink + "\" class=\"badge badge-success\" target=\"_blank\">" + row.CompletedForms[i].CategoryName + "</a>"
                        spanList += span;

                    }



                    return spanList;
                }

            },
            { data: "CreatedDateToString" },
            {
                'data': 'Action', "bSortable": false, 'render': function (data, type, row, meta) {

                    var pendiggData = "";
                    var i;
                    for (i = 0; i < row.UserFilesList.length; i++) {



                        pendiggData += "<a target=\"_blank\" href=\"" + row.UserFilesList[i].FileURL + "\" class=\"badge badge-primary\" title=\"View File\">" + row.UserFilesList[i].Name + "</a>";


                    }

                    return pendiggData;



                }
            },
            {
                'data': 'Action', "bSortable": false, 'render': function (data, type, row, meta) {
                    var UserId = row.Id;
                    var sendQuestLink = "/Staff/Index/" + UserId;
                    var ansLink = "/Staff/UserAnswer/" + UserId;
                    var link = "";
                    link += "<a href=\"" + sendQuestLink + "\" class=\"btn btn-sm btn-danger\">Send Question</a>";
                    link += "<a href=\"" + ansLink + "\" class=\"btn btn-sm btn-danger\">Check Reply</a>";

                    return link;


                    return link;
                }
            }

        ]
    })




});




