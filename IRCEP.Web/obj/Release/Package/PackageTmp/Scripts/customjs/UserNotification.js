﻿$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "/Usernotification/QuestionList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {

            var childData = JSON.parse(response.data);

            var htmlleft = "";
            var htmlright = "";
            htmlMsg = "";

            var i;
            var j;

            for (i = 0; i < childData.length; i++) {

                var uRole = childData[i].UserRole;
                if (uRole != 'Pregnant') {

                    var datetime = moment(new Date(childData[i].CreatedDate)).format("MMM") + ' ' + moment(new Date(childData[i].CreatedDate)).format("DD") + ' ' + moment(new Date(childData[i].CreatedDate)).format("hh:mm A");

                    htmlleft += '<div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">' + childData[i].FullName + '</span><span style="padding-left: 10px" class="direct-chat-timestamp float-right">' + datetime + '</span> </div><div class="direct-chat-text"> ' + childData[i].Question + '</div>';
                    debugger


                    for (j = 0; j < childData[i].Answers.length; j++) {

                        var answerlst = childData[i].Answers;

                        var datetime = moment(new Date(answerlst[j].CreatedDate)).format("MMM") + ' ' + moment(new Date(answerlst[j].CreatedDate)).format("DD") + ' ' + moment(new Date(answerlst[j].CreatedDate)).format("hh:mm A");


                        htmlleft += '<div  class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">' + answerlst[j].FullName + '</span><span style="padding-left: 10px" class="direct-chat-timestamp float-left">' + datetime + '</span></div><div class="direct-chat-text">' + answerlst[j].Answer + '</div> </div>';
                        //
                        $('#hdduserName').val(answerlst[j].FullName);
                    }

                    $('#hddQuestionId').val(childData[i].Id);
                }
            }
            htmlMsg = '<div class="card direct-chat direct-chat-primary"><div class="card-body"> <div id="divmsg" style="height:400px" class="direct-chat-messages">';
            htmlMsg += '<div class="direct-chat-msg">' + htmlleft;

            htmlMsg += '</div><div id="divrightSide" class="direct-chat-msg right"></div> ';

            htmlMsg += '</div></div></div></div> <div class="card-footer"><div class="input-group"><input type="text" id="txtMsg" name="txtMsg" placeholder="Type Message ..." class="form-control"><span class="input-group-append"><button type="button"  onclick="replyMessage();" class="btn btn-primary">Reply</button></span></div></div>';


            $('#divChat').append(htmlMsg);

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });


    $("#divmsg").animate({
        scrollTop: $('#divmsg')[0].scrollHeight - $('#divmsg')[0].clientHeight
    }, 1000);

});

function replyMessage() {

    var obj = {};
    obj.QuestionId = $('#hddQuestionId').val();
    obj.mesaage = $('#txtMsg').val();

    if (obj.mesaage.trim() == '') {
        alert('Please write message!');
    } else {


        $.ajax({
            type: "POST",
            url: "/Usernotification/UserReply",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(obj),
            async: false,
            success: function (response) {


                var htmlright = '<div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">' + $('#hdduserName').val() + '</span><span style="padding-left: 10px" class="direct-chat-timestamp float-left"></span></div><div class="direct-chat-text">' + $('#txtMsg').val() + '</div>';

                $('#divrightSide').append(htmlright);

                $('#txtMsg').val('')
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });

    }
}