﻿$(document).ready(function () {
    $.noConflict();



    $('#customTable').DataTable({
        "ajax": {
            "dataType": 'json',
            "type": "GET",
            "url": '/ApplicationUsers/GetAllUser',
            "dataSrc": function (json) {
                
                $('#lblTotalCount').text(json.TotalCount);
                $('#lblTotalPregnants').text(json.TotalPregnants);
                $('#lblTotalNonPregnants').text(json.TotalNonPregnants);

                return json.Items;
            }
        },
        columns: [
            { data: "FirstName" },
            { data: "UserNumber" },
            { data: "RegisterCountry" },
            { data: "Country" },
            {
                data: "PendingForms",
                render: function (data, type, row, meta) {
                    var pendiggData = "";
                    var i;
                    for (i = 0; i < row.PendingForms.length; i++) {

                        pendiggData += '<span class="badge badge-warning">' + row.PendingForms[i].CategoryName + '</span>';


                    }

                    return pendiggData;
                }

            },
            {
                data: "CompletedForms",
                render: function (data, type, row, meta) {

                    var pendiggData = "";
                    var i;
                    for (i = 0; i < row.CompletedForms.length; i++) {

                        pendiggData += '<span class="badge badge-success">' + row.CompletedForms[i].CategoryName + '</span>';


                    }

                    return pendiggData;
                }

            },

            {
                data: "PhoneNumberConfirmed",
                render: function (data, type, row, meta) {

                    if (row.PhoneNumberConfirmed) {
                        return '<span class="badge badge-success">Verified</span>';
                    } else {
                        return '<span class="badge badge-danger">Unverified</span>';
                    }

                }

            },

            {
                data: "ConsentSign",
                render: function (data, type, row, meta) {

                    if (row.ConsentSign) {
                        return '<span class="badge badge-success">Verified</span>';
                    } else {
                        return '<span class="badge badge-danger">Unverified</span>';
                    }

                }

            },
            { data: "CreatedDateToString" },
            {
                'data': 'Action', "bSortable": false, 'render': function (data, type, row, meta) {
                    var UserId = row.Id;
                    var deleteAnchorLink = "/ApplicationUsers/Delete?id=" + UserId;
                    var link = "<button type=\"button\" class=\"btn btn-sm btn-primary\" onclick=\"handleSendMessage('" + UserId + "')\">Send</button>";
                    link += "<a href=\"" + deleteAnchorLink + "\" class=\"btn btn-sm btn-danger\">Delete</a>";



                    return link;
                }
            }
        ]
    })




});




